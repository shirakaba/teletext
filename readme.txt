== DIRECTORY STRUCTURE ==
The directory containing the whole project (teletextproj/) should contain, on the same level:
1) assets/
2) src/
3) include/
4) lib/
5) makefile
6) readme.txt

== TO MAKE ==
Ensure that you are cd'ed into the above-mentioned teletextproj directory.
Run the following command:
make

== TO RUN THE UNIT TESTS FOR THE TELETEXT & EXTENSION PROGRAMS ==
Run the following command:
bin/Test

== TO RUN THE TELETEXT PROGRAM UPON THE TESTFILE ==
After making the project, ensure that you are cd'ed into teletextproj.
Run the following command:
bin/Teletext bin/assets/test.m7

== TO RUN THE TELETEXT PROGRAM UPON OTHER FILES ==
After making the project, copy your own mode7 file (ie. myfile.m7) into the assets folder residing inside teletextproj/bin.
Now ensure that you are cd'ed into the above-mentioned 'mydir' directory.

Run the following command:
bin/Teletext bin/assets/myfile.m7

== TO RUN THE EXTENSION PROGRAM ==
After making the project, ensure that you are cd'ed into teletextproj.

Run the following command:
bin/Extension

== TO CLEAN ==
After making the project, ensure that you are cd'ed into the above-mentioned 'teletextproj' directory.
Run the following command:
make clean
