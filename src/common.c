#include "common.h"

/* Validates whether argument count matches expected count.
 * Returns '0' if valid. Else, '1'. */
FILE** getErrorStream()
{
  static FILE* fpp;

  return &fpp;
}

void setErrorStream(FILE* proposed)
{
  FILE** fpp = getErrorStream();
  *fpp = proposed;
}

int validateArgCount(int argc, int ARGS_NEEDED)
{
  if(argc != ARGS_NEEDED){
    fprintf(*getErrorStream(),"Inadequate arguments (%d); expected %d arguments.\n",
        argc, ARGS_NEEDED);
    fflush(*getErrorStream());
    return 1;
  }
  return 0;
}


/* Exits the program if an int test returned positive. */
void exitIfIntTestAboveZero(int result)
{
  if(result > 0){
    exit(1);
  };
}


/* Exits the program if a pointer test returned positive. */
void exitIfPointerTestFailed(void *result)
{
  if(result == NULL){
    exit(1);
  };
}


/* Validates whether hex file passed in is: a .m7 file; of non-null filepath;
 * and has exactly 1000 characters.
 * Returns file pointer if all valid. Else, NULL. */
FILE* validateHexFile(char *filePath)
{
  FILE *pHexFileFilepath = NULL;
  long int stringLength;
  size_t lastCharOfFilePath = strlen(filePath);
  char* fileExtensionStart = &filePath[lastCharOfFilePath - CHARACTERS_IN_FILE_EXTENSION];

  if(strcmp(fileExtensionStart, ".m7")){
    fprintf(*getErrorStream(), "File '%s' does not have expected extension '.m7'.\n",
    filePath);
    fflush(*getErrorStream());
    return NULL;
  }


  if((pHexFileFilepath = fopen(filePath, "rb")) == NULL){
    fprintf(*getErrorStream(),"Cannot open file '%s'.\n", filePath);
    fflush(*getErrorStream());
    return NULL;
  }

  fseek(pHexFileFilepath, 0, SEEK_END);
  stringLength = ftell(pHexFileFilepath);
  rewind(pHexFileFilepath);

  if(stringLength != ROWS * COLUMNS){
    fprintf(*getErrorStream(), "stringLength %li didn't match expected number of"\
        " characters (%d).\n", stringLength,
            ROWS * COLUMNS);
    fflush(*getErrorStream());
    return NULL;
  }

  return pHexFileFilepath;
}


/* Reads the hexFile into a string, then closes the hex file.
 * Returns string unless: 1) malloc failed; or 2) fread failed, in which cases it
 * returns NULL. */
unsigned char *readHexFileIntoString(unsigned char rawPage[ROWS * COLUMNS],
    FILE *pHexFileFilepath)
{
  size_t bytesRead = 0;

  bytesRead = fread(rawPage, sizeof(unsigned char), ROWS * COLUMNS, pHexFileFilepath);

  if(ferror(pHexFileFilepath) || !bytesRead){
    perror("fread");
    return NULL;
  }

  if(bytesRead != (ROWS * COLUMNS)){
    fprintf(*getErrorStream(), "Read %d bytes from file rather than required %d\n",
        (int)bytesRead, ROWS * COLUMNS);
    fflush(*getErrorStream());
    return NULL;
  }

  fclose(pHexFileFilepath);

  return rawPage;
}


/* Populates an array with the characters of a string, ensuring those characters
 * always include a parity bit.
 * Returns 0 in all cases currently. */
void populateArrayWithHexString(unsigned char *hexFileAsString,
    unsigned char array[ROWS][COLUMNS])
{
  int i = 0, j = 0, stringPos = 0;
  unsigned char c = 0;

  while(i < ROWS * COLUMNS){
    c = hexFileAsString[stringPos];

    c |= PARITY_BIT;

    array[j][i % COLUMNS] = c;
    i++;

    if(i % COLUMNS == 0){
      j++;
    }
    stringPos++;
  };
}


/* Returns '1' if SDL_Init succeeds.
 * Else, '1'. */
int SDL_InitWrapper(SDL_Simplewin* sw)
{
  int result = SDL_Init(SDL_INIT_VIDEO);

  if(result != 0){
    fprintf(*getErrorStream(), "Could not initialise SDL: %s\n", SDL_GetError());
    fflush(*getErrorStream());
    SDL_Quit();
    return 1;
  }

/*  sw->finished = SDL_FALSE; */

  sw->win= SDL_CreateWindow("SDL Window",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            BODY_WIDTH, BODY_HEIGHT,
                            SDL_WINDOW_SHOWN);
  if(sw->win == NULL){
    fprintf(*getErrorStream(), "\nUnable to initialize SDL Window:  %s\n", SDL_GetError());
    fflush(*getErrorStream());
    SDL_Quit();
    return 1;
  }

  sw->renderer = SDL_CreateRenderer(sw->win, -1, SDL_RENDERER_TARGETTEXTURE|SDL_RENDERER_SOFTWARE);
  if(sw->renderer == NULL){
    fprintf(*getErrorStream(), "\nUnable to initialize SDL Renderer:  %s\n", SDL_GetError());
    fflush(*getErrorStream());
    SDL_Quit();
    return 1;
  }

  /* Chooses the colour for the rendering */
  SDL_SetRenderDrawColor(sw->renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
  /* Overwrites the entire buffer with the selected color. */
  SDL_RenderClear(sw->renderer);
  /* Draws the render buffer to the screen. */
  SDL_RenderPresent(sw->renderer);

  return 0;
}


/* Generates a zero-padded string filename.
 * Uncharacterised behaviour if digitsOfID < i.
 * Returns the string if: digitsOfID is between 1 & 9; malloc for newFilePath
 * succeeds; and strFromInt creation with sprintf succeeds. Else: NULL.*/
char* generateStrFromInt(int i, int digitsOfID, ImgOrHexfile imgOrHexfile)
{
  int result, radix = DECIMAL;
  size_t strFromIntSize, strFromIntLen;
  char *strFromInt = NULL, imgFilename[] = "%0?d.gif", hexFilename[] = "%0?d.m7",
  *assetFilename;

  switch(imgOrHexfile){
    case HEXFILE:
    assetFilename = hexFilename;
      break;
    case IMG:
    assetFilename = imgFilename;
      break;
    default:
      fprintf(*getErrorStream(), "ImgOrHexfile enum not recognised in"\
      " generateStrFromInt().\n");
      return NULL;
  }

  if(digitsOfID < 1 || digitsOfID >= radix){
    fprintf(*getErrorStream(), "digitsOfID specified (%d) was out of range."\
        " Please enter between 1 and 9.\n", digitsOfID);
    return NULL;
  }

  strFromIntLen = sizeof(char) * (digitsOfID + CHARS_IN_IMG_FILE_EXTENSION);
  if(imgOrHexfile == HEXFILE){
    strFromIntLen = sizeof(char) * (digitsOfID + CHARS_IN_HEXFILE_EXTENSION);
  }
  strFromIntSize = strFromIntLen + sizeof(char) * LENGTH_OF_NULLCHAR;
  strFromInt = malloc(strFromIntSize);

  if(strFromInt == NULL){
    fprintf(*getErrorStream(), "malloc for newFilePath failed.\n");
    return NULL;
  }

  assetFilename[POS_DECIDING_NO_OF_DIGITS_TO_PAD_BY] = (char)('0' + digitsOfID);

  result = sprintf(strFromInt, assetFilename, i);

  if(result < 0){
    fprintf(*getErrorStream(), "Sprintf failed in generateStrFromInt.\n");
    return NULL;
  }

  return strFromInt;
}


/* Generates triple-digit numbered filepaths to assets in a folder specified by basePath.
 * Returns newFilePath if generateStrFromInt() and malloc of newFilePath succeed. Else, NULL. */
char* generateFilePath(unsigned int i, char *basePath, ImgOrHexfile imgOrHexfile)
{
  char *newFilePath, *presult, *strFromInt = NULL;
  size_t basePathlen, basePathSize, strFromIntSize, strFromIntLen;

  basePathlen = strlen(basePath);
  basePathSize = sizeof(char) * (basePathlen + LENGTH_OF_NULLCHAR);

  if(imgOrHexfile == IMG){
    presult = strFromInt = generateStrFromInt(i, THREE_DIGIT_ID, IMG);
  }
  else{
    presult = strFromInt = generateStrFromInt(i, THREE_DIGIT_ID, HEXFILE);
  }
  if(presult == NULL){
    fprintf(*getErrorStream(), "Unable to generate the three-character string from"\
        " the input integer %d.\n", i);
    return NULL;
  }

  strFromIntLen = strlen(strFromInt);
  strFromIntSize = strFromIntLen + sizeof(char) * LENGTH_OF_NULLCHAR;

  newFilePath = malloc(basePathlen + strFromIntSize);

  if(newFilePath == NULL){
    fprintf(*getErrorStream(), "malloc for newFilePath failed.\n");
    return NULL;
  }

  /* strncpy copies NULLCHAR across if asked to, which we do in this case. */
  strncpy(newFilePath, basePath, basePathSize);
  /* Not clear whether strncat copies NULLCHAR, so doing it manually below. */
  strncat(newFilePath, strFromInt, strFromIntLen);


  newFilePath[basePathlen + strFromIntLen] = NULLCHAR;
  free(strFromInt);
  /* in the current method, basePath was never allocated, so needn't be freed. */

  return newFilePath;
}


/* Populates the imageFont with images from a requested folder.
 * Returns RESUMED_GFX_GLYPH_FILENUMBER if folderToLoadFrom == LOAD_FROM_ALPHA_FOLDER.
 * Returns GLYPHS_NEEDED if folderToLoadFrom == LOAD_FROM_GFX_FOLDER. */
unsigned int loadGlyphsFromFolderIntoImageFont(SDL_Texture* imageFont[GLYPHS_NEEDED],
    SDL_Simplewin* sw, unsigned int arrayPosToLoadInto,
    WhichAssetFolderToLoadFrom folderToLoadFrom)
{
  unsigned int fileNumber;
  SDL_Surface *surface;
  char *imgFilepath, *basePath = BASEPATH_TO_ALPHA;
  void *presult;

  if(folderToLoadFrom == LOAD_FROM_GFX_FOLDER){
    basePath = BASEPATH_TO_GFX;
  }

  for(fileNumber = FIRST_PRINTABLE_GLYPH; fileNumber <= HIGHEST_GLYPH_CODE; arrayPosToLoadInto++,
      fileNumber++){

    if(folderToLoadFrom == LOAD_FROM_GFX_FOLDER && fileNumber == FIRST_SHARED_GLYPH_FILENUMBER){
      fileNumber = RESUMED_GFX_GLYPH_FILENUMBER;
    }

    presult = imgFilepath = generateFilePath(fileNumber, basePath, IMG);
    exitIfPointerTestFailed(presult);

    presult = surface = IMG_LoadToSurface(imgFilepath, DONT_SET_WHITE_AS_ALPHA);
    exitIfPointerTestFailed(presult);

    free(imgFilepath);

    /* Freeing the surface occurs inside this function. */
    presult = imageFont[arrayPosToLoadInto] = surfaceToTexture(sw, surface);
    exitIfPointerTestFailed(presult);
  }

  /* printf("Just entered into imageFont[%d]\n", arrayPosToLoadInto - 1); */

  return arrayPosToLoadInto;
}


/* Populates the imageFont with images from the alpha folder, then from the gfx folder.
 * Returns 0 if as many images were found in each folder as there were slots expected
 * to be filled in the array. Else, 1. */
int populateImageFont(SDL_Texture **imageFont, SDL_Simplewin *sw)
{
  unsigned int arrayPosToLoadInto = 0, result;

  result = arrayPosToLoadInto = loadGlyphsFromFolderIntoImageFont(imageFont, sw,
      arrayPosToLoadInto, LOAD_FROM_ALPHA_FOLDER);
  if(result != RESUMED_GFX_GLYPH_FILENUMBER){
    fprintf(*getErrorStream(), "Last loaded into imageFont[%d]; had expected %d.\n",
        result - 1, RESUMED_GFX_GLYPH_FILENUMBER - 1);
    return 1;
  }

  result = loadGlyphsFromFolderIntoImageFont(imageFont, sw, arrayPosToLoadInto,
      LOAD_FROM_GFX_FOLDER);
  if(result != GLYPHS_NEEDED){
    fprintf(*getErrorStream(), "Last loaded into imageFont[%d]; had expected %d.\n",
        result - 1, GLYPHS_NEEDED - 1);
    return 1;
  }

  return 0;
}


/* Can't load image directly into the more useful 'SDL texture', so need to follow
 * this function with surfaceToTexture().
 * Returns SDL_Surface pointer if loading image into SDL_Surface is successful;
 * else, NULL. */
SDL_Surface* IMG_LoadToSurface(char *filepath, SetWhiteToBeAlphaChannel setWhiteAsAlpha)
{
  SDL_Surface *surface = IMG_Load(filepath);

  if(surface == NULL) {
    printf( "Unable to load %s! Consider rebuilding. SDL_image Error: %s\n", filepath,
        IMG_GetError() );
    return NULL;
  }

  /* Sets white to be completely transparent if setWhiteToAlpha == 1. */
  if(setWhiteAsAlpha == DO_SET_WHITE_AS_ALPHA){
    SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, FULL_RGB, FULL_RGB,
        FULL_RGB));
  }

  return surface;
}


/* Returns SDL_Texture pointer if creating texture from SDL_Surface is successful;
 * else, NULL. */
SDL_Texture* surfaceToTexture(SDL_Simplewin* sw, SDL_Surface* surface)
{
  SDL_Texture *texture;

  texture = SDL_CreateTextureFromSurface(sw->renderer, surface);

  if(texture == NULL){
    printf("Texture conversion has failed: %s\n", SDL_GetError());
    return NULL;
  }

  SDL_FreeSurface(surface);

  return texture;
}


/* Initialises every cell in an array of codes to the default style. */
/* The styles for each character in the teletext page are stored as binary values,
 * into styles[][], in this format:
 * [bits are shown set to the default style of white text (alpha & gfx),
 * single height, contig gfx, black bg, and release gfx]
 *   0   0   0   0    0  0  0  0    0  1  1  1    0  1  1  1
 *  N/A N/A gfx chg   hg br bg bb   sg gr gg gb   dh ar ag ab
 *
 *  ab: alpha blue channel
 *  ag: alpha green channel
 *  ar: alpha red channel
 *  dh: double height (if 1; else, single height)
 *
 *  gb: gfx blue channel
 *  gg: gfx green channel
 *  gr: gfx red channel
 *  sg: separated gfx (if 1; else, contig gfx)
 *
 *  bb: background blue channel
 *  bg: background green channel
 *  br: background red channel
 *  hg: hold gfx (if 1; else, release gfx)
 *
 *  chg: clear held gfx (if 1; else, retain the presently held gfx)
 *  gfx: gfx mode (if 1; else, alpha mode).
 *  N/A: unused
 *  N/A: unused
 * */
void setStylesToDefault(unsigned short styles[ROWS][COLUMNS])
{
  int currentColumn = 0, currentRow = 0;
/* DEFAULT_CODE is 0b1110111 (as detailed above), but binary constants are a GCC
 * extension, so I've converted to decimal. */

  while(currentColumn < ROWS * COLUMNS){
    styles[currentRow][currentColumn % COLUMNS] = DEFAULT_CODE;
    currentColumn++;

    if(currentColumn % COLUMNS == 0){
      currentRow++;
    }
  };
}


unsigned short convertNoOfBitsToBitPattern(BitsInRegion bitsInRegion, unsigned char
    radix)
{
  unsigned short bitPatternToMask = 0;
  unsigned char i;

  for(i = 0; i < bitsInRegion; i++){
    bitPatternToMask += (unsigned short)pow(radix, i);
  }
  return bitPatternToMask;
}


/* Returns the bitpattern of the current character colour (ie. 0b101), shifted to
 * wherever the A_RGB region is
 * (by default, the A_RGB region is conveniently the lowest 3 bits, so a shift of
 * zero is applied).*/
unsigned short getRGBBitPattern(unsigned short currentStyle, ShiftToRegion shiftToWhichRegion)
{
  unsigned short RGBBitPattern = WHITE;

  RGBBitPattern <<= shiftToWhichRegion;
  RGBBitPattern &= currentStyle;

  return RGBBitPattern;
}


ControlCodeInputs getControlCodeInput(unsigned char controlCode)
{
  switch (controlCode) {
    case A_RED:
    case GFX_RED:
      return RED;
    case A_GREEN:
    case GFX_GREEN:
      return GREEN;
    case A_YELLOW:
    case GFX_YELLOW:
      return YELLOW;
    case A_BLUE:
    case GFX_BLUE:
      return BLUE;
    case A_MAGENTA:
    case GFX_MAGENTA:
      return MAGENTA;
    case A_CYAN:
    case GFX_CYAN:
      return CYAN;
    case A_WHITE:
    case GFX_WHITE:
      return WHITE;
    case BG_BLACK:
      return BLACK;
    case BG_NEW:
      return NEW_BG_REQUESTED;
    case DOUBLE_HEIGHT:
    case SEP_GFX:
    case HOLD_GFX:
      return TOGGLE_ON;
    case SINGLE_HEIGHT:
    case CONTIG_GFX:
    case RELEASE_GFX:
      return TOGGLE_OFF;
    default:
      return NULL_INPUT;
  }
}


BitsInRegion getBitsInRegion(unsigned char controlCode)
{
  switch (controlCode) {
    case SINGLE_HEIGHT:
    case DOUBLE_HEIGHT:
    case CONTIG_GFX:
    case SEP_GFX:
    case HOLD_GFX:
    case RELEASE_GFX:
      return SINGLE_BIT_REGION;
    case A_RED:
    case A_GREEN:
    case A_YELLOW:
    case A_BLUE:
    case A_MAGENTA:
    case A_CYAN:
    case A_WHITE:
    case GFX_RED:
    case GFX_GREEN:
    case GFX_YELLOW:
    case GFX_BLUE:
    case GFX_MAGENTA:
    case GFX_CYAN:
    case GFX_WHITE:
    case BG_BLACK:
    case BG_NEW:
      return RGB_REGION;
    default:
      return NULL_REGION;
  }
}


ShiftToRegion getRegionToShiftTo(unsigned char controlCode)
{
  switch (controlCode) {
    case A_RED:
    case A_GREEN:
    case A_YELLOW:
    case A_BLUE:
    case A_MAGENTA:
    case A_CYAN:
    case A_WHITE:
      return SHIFT_TO_A_RGB_REGION;
    case DOUBLE_HEIGHT:
    case SINGLE_HEIGHT:
      return SHIFT_TO_D_HEIGHT_REGION;
    case GFX_RED:
    case GFX_GREEN:
    case GFX_YELLOW:
    case GFX_BLUE:
    case GFX_MAGENTA:
    case GFX_CYAN:
    case GFX_WHITE:
      return SHIFT_TO_GFX_RGB_REGION;
    case SEP_GFX:
    case CONTIG_GFX:
      return SHIFT_TO_SEP_GFX_REGION;
    case BG_BLACK:
    case BG_NEW:
      return SHIFT_TO_BG_RGB_REGION;
    case HOLD_GFX:
    case RELEASE_GFX:
      return SHIFT_TO_HOLD_GFX_REGION;
    default:
      return SHIFT_TO_NULL_REGION;
  }
}


SetBitTools generateToolsForSettingStylesBit(unsigned short input, BitsInRegion bitsInRegion,
    ShiftToRegion bitShift)
{
  SetBitTools setBitTools;
  unsigned char radix = BINARY;
  unsigned short inputBitPattern;

  inputBitPattern = convertNoOfBitsToBitPattern(bitsInRegion, radix);
  inputBitPattern <<= bitShift;
  setBitTools.inputBitMask = ~inputBitPattern;

  input <<= bitShift;
  setBitTools.inputShifted = input;

  return setBitTools;
}


void setRow(unsigned short styles[ROWS][COLUMNS], int currentRow, int currentColumn,
    StyleFromHexCode styleFromHexCode)
{
  SetBitTools toolsForSettingBitsOfInput, toolsForSettingBitsOfAlphaOrGfxMode;

  /* Will set the incoming style, then toggle into Alpha or Gfx mode according to
   * which control code was just received. */
  toolsForSettingBitsOfInput = generateToolsForSettingStylesBit(styleFromHexCode.input,
    styleFromHexCode.bitsInRegion, styleFromHexCode.bitShift);
  toolsForSettingBitsOfAlphaOrGfxMode =
    generateToolsForSettingStylesBit((unsigned short)styleFromHexCode.alphaOrGfxMode,
    SINGLE_BIT_REGION, SHIFT_TO_GFX_MODE_REGION);

  while(currentColumn < COLUMNS){
    setCell(styles, currentRow, currentColumn, toolsForSettingBitsOfInput,
        toolsForSettingBitsOfAlphaOrGfxMode);
    currentColumn++;
  };

}


unsigned short setCell(unsigned short styles[ROWS][COLUMNS], int currentRow,
    int currentColumn, SetBitTools toolsForSettingBitsOfInput,
    SetBitTools toolsForSettingBitsOfAlphaOrGfxMode)
{
  unsigned short currentCellOutputCode;

  setStyleBits(styles, currentRow, currentColumn, toolsForSettingBitsOfInput.inputBitMask,
    toolsForSettingBitsOfInput.inputShifted);
  setStyleBits(styles, currentRow, currentColumn, toolsForSettingBitsOfAlphaOrGfxMode.inputBitMask,
    toolsForSettingBitsOfAlphaOrGfxMode.inputShifted);

  currentCellOutputCode = styles[currentRow][currentColumn];

  return currentCellOutputCode;
}


unsigned short setStyleBits(unsigned short styles[ROWS][COLUMNS], int currentRow,
    int currentColumn, unsigned short inputBitMask, unsigned short input)
{
  unsigned short currentCellOutputCode;

  styles[currentRow][currentColumn] &= inputBitMask;
  styles[currentRow][currentColumn] |= input;

  currentCellOutputCode = styles[currentRow][currentColumn];

  return currentCellOutputCode;
}


CCodeActivity getActivityOfControlCode(unsigned char currentHexCode)
{
  switch (currentHexCode){
    case A_RED:
    case A_GREEN:
    case A_YELLOW:
    case A_BLUE:
    case A_MAGENTA:
    case A_CYAN:
    case A_WHITE:
    case GFX_RED:
    case GFX_GREEN:
    case GFX_YELLOW:
    case GFX_BLUE:
    case GFX_MAGENTA:
    case GFX_CYAN:
    case GFX_WHITE:
    case SINGLE_HEIGHT:
    case DOUBLE_HEIGHT:
    case CONTIG_GFX:
    case SEP_GFX:
    case BG_BLACK:
    case BG_NEW:
    case HOLD_GFX:
    case RELEASE_GFX:
      return IS_ACTIVE_CONTROL_CODE;
    default:
      return IS_NOT_ACTIVE_CONTROL_CODE;
  }
}


void traversePageToGetStylesFromHexArray(unsigned char frameBuffer[ROWS][COLUMNS],
                                         unsigned short styles[ROWS][COLUMNS])
{
  int currentColumn = 0, currentRow = 0, i = 0;
  unsigned char currentHexCode;
  CCodeActivity cCodeActivity;
  unsigned short currentStyle;
  SetBitTools toolsForSettingClearHeldGfxBit;
  StyleFromHexCode styleFromHexCode;

  while (i < ROWS * COLUMNS) {
    currentColumn = i % COLUMNS;
    currentStyle = styles[currentRow][currentColumn];
    currentHexCode = frameBuffer[currentRow][currentColumn];
    if(currentHexCode < HEXCODE_OF_FIRST_PRINTABLE_GLYPH_WITH_PARITY) {
      styleFromHexCode = getStyleFromHexCode(currentHexCode, currentStyle);
      toolsForSettingClearHeldGfxBit = generateToolsForSettingStylesBit(styleFromHexCode.clearHeldGfx,
        SINGLE_BIT_REGION, SHIFT_TO_CLEAR_HELD_GFX_REGION);
      setStyleBits(styles, currentRow, currentColumn, toolsForSettingClearHeldGfxBit.inputBitMask,
        toolsForSettingClearHeldGfxBit.inputShifted);

      cCodeActivity = getActivityOfControlCode(currentHexCode);
      if(cCodeActivity == IS_ACTIVE_CONTROL_CODE){
        setRow(styles, currentRow, currentColumn, styleFromHexCode);
      }
    }
    i++;

    if(i % COLUMNS == 0){
      currentRow++;
    }
  };
}


/* Returns 'MODE_ENABLED' if the currentHexCode enables double-height mode.
 * Returns 'MODE_DISABLED' if the currentHexCode requests single-height mode.
 * Else, returns 'NO_CHANGE_IN_MODE' to convey that no change from the current
 * single/double height mode need be registered. */
Mode getIncomingHeightMode(unsigned char currentHexCode)
{
  Mode heightModeToggle;

  switch(currentHexCode){
    case SINGLE_HEIGHT:
      heightModeToggle = MODE_DISABLED;
      break;
    case DOUBLE_HEIGHT:
      heightModeToggle = MODE_ENABLED;
      break;
    default:
      heightModeToggle = NO_CHANGE_IN_MODE;
  }

  return heightModeToggle;
}


/* Returns 'MODE_ENABLED' if the currentHexCode concerns gfx controls.
 * Returns 'MODE_DISABLED' if the currentHexCode concerns alpha controls.
 * Else, returns 'NO_CHANGE_IN_MODE' to convey that no change from the current
 * gfx/alpha mode need be registered. */
Mode getIncomingGfxMode(unsigned char currentHexCode)
{
  Mode gfxToggle;

  switch(currentHexCode){
    case GFX_RED:
    case GFX_GREEN:
    case GFX_YELLOW:
    case GFX_BLUE:
    case GFX_MAGENTA:
    case GFX_CYAN:
    case GFX_WHITE:
      /* To match Neill's implementation of separated graphics in test.m7, all
       * control codes concerning graphics transition you into gfx mode, avoiding
       * the display of an alpha character after the word 'Separated'. */
    case SEP_GFX:
    case CONTIG_GFX:
    case RELEASE_GFX:
    case HOLD_GFX:
      gfxToggle = MODE_ENABLED;
      break;
    case A_RED:
    case A_GREEN:
    case A_YELLOW:
    case A_BLUE:
    case A_MAGENTA:
    case A_CYAN:
    case A_WHITE:
      gfxToggle = MODE_DISABLED;
      break;
    default:
      gfxToggle = NO_CHANGE_IN_MODE;
  }

  return gfxToggle;
}


/* Returns the currentHexCode's requested mode for one of alpha/gfx or single/double
 * height. */
/* Returns -1 if no mode-sensitive control codes were issued. */
char getIncomingMode(ModePassedByCurrentHexCode modePassedByCurrentHexCode,
    unsigned char currentHexCode, unsigned char currentMode)
{
  Mode result;

  switch(modePassedByCurrentHexCode) {
    case ALPHA_OR_GFX_MODE:
      result = getIncomingGfxMode(currentHexCode);
      break;
    case HEIGHT_MODE:
      result = getIncomingHeightMode(currentHexCode);
      break;
    default:
      result = NO_CHANGE_IN_MODE;
      break;
  }

  /* If hexCode that doesn't change the gfxMode was issued (result == NO_CHANGE_IN_MODE),
   * leave currentMode as-is. */
  if(result != NO_CHANGE_IN_MODE){
    currentMode = (unsigned char)result;
  }

  return currentMode;
}


/* Expects zeroed array to contain codes. */
/* To test: ensure that only controlCodes between 0x80 and 0x9F satisfy the if condition.
 * Ensure that any cell with controlCode == 8D (and all cells before that with
 * controlCode == 8C) has a 1 at its fourth bit, like: 0b1000 */
StyleFromHexCode getStyleFromHexCode(unsigned char currentHexCode,
    unsigned short currentStyle)
{
  StyleFromHexCode styleFromHexCode;
  unsigned short input;
  char newAlphaOrGfxMode = 0, newHeightMode = 0;
  unsigned char currentGfxAlphaMode, currentHeightMode, clearHeldGfx = 1;

  /* If cell contains a control code */

  input = getControlCodeInput(currentHexCode);
  if (input == NEW_BG_REQUESTED) {
    input = getRGBBitPattern(currentStyle, SHIFT_TO_A_RGB_REGION);
  }

  currentGfxAlphaMode = getStyle(SHIFT_TO_GFX_MODE_REGION, currentStyle);
  currentHeightMode = getStyle(SHIFT_TO_D_HEIGHT_REGION, currentStyle);

  newAlphaOrGfxMode = getIncomingMode(ALPHA_OR_GFX_MODE, currentHexCode,
    currentGfxAlphaMode);
  newHeightMode = getIncomingMode(HEIGHT_MODE, currentHexCode, currentHeightMode);

  /* If both gfxmode and heightmode are unchanged by this control code, don't
   * clear the held gfx character. */
  if(newAlphaOrGfxMode == currentGfxAlphaMode && newHeightMode == currentHeightMode){
    clearHeldGfx = 0;
  }

  styleFromHexCode.input = input;
  styleFromHexCode.bitsInRegion = getBitsInRegion(currentHexCode);
  styleFromHexCode.bitShift = getRegionToShiftTo(currentHexCode);
  styleFromHexCode.alphaOrGfxMode = (unsigned char)newAlphaOrGfxMode;
  styleFromHexCode.clearHeldGfx = clearHeldGfx;

  return styleFromHexCode;
}


/* Assumes hexCode has parity bit included. */
unsigned char translateHexCodeToFontIndex(unsigned char hexCode,
    unsigned char graphicsModeOn)
{
  unsigned char fontIndex;

  if(hexCode < START_PRINTABLE_HEXCODES){
    /* Still deciding where to handle translation of controlcodes;
     * note that hold graphics retains the last graphic, rather than using space.
     * Could make release graphics always 'hold' space during control codes,
     * while hold graphics would hold the previous graphic to be placed. */
    fontIndex = FONT_INDEX_FOR_SPACE;
    return fontIndex;
  }

  /* Test: if hexCode is A0 (160), fontIndex should be 160 - 128 - 32 == 0*/
  fontIndex =  (unsigned char)(hexCode - PARITY_BIT - FIRST_PRINTABLE_GLYPH);

  if(graphicsModeOn){
    if(hexCode >= FIRST_SHARED_GLYPH_HEXCODE && hexCode < RESUMED_GFX_GLYPH_HEXCODE){
    /* fontIndex doesn't change here because fontIndex[C0 to DF] is shared with
     * the alpha glyphs */
    }
    else {
      fontIndex += TOTAL_GLYPHS_IN_MODE;

      /* There are 32 fewer unique glyphs in GFX mode, so these shared glyphs must
       * be accounted for in the fontIndex. */
      if(hexCode >= RESUMED_GFX_GLYPH_HEXCODE){
        fontIndex -= RESUMED_GFX_GLYPH_HEXCODE - FIRST_SHARED_GLYPH_HEXCODE;
      }

      /* I repurposed the 'empty sixel' glyph in gfx mode for a 'grid' image to
       * overlay during separated graphics,
       * so the 'empty sixel' glyph must instead return an identical 'space' glyph
       * from alpha mode. */
      if(hexCode == HEXCODE_FOR_SPACE){
        fontIndex = FONT_INDEX_FOR_SPACE;
      }
    }

  }

/*  printf("From hexCode [%x], fontIndex is: [%d]\n", hexCode, fontIndex); */

  if(fontIndex >= GLYPHS_NEEDED){
    fprintf(*getErrorStream(), "The hexcode generated (%x) was beyond the range"\
      " of the imageFont.\n", hexCode);
    fflush(*getErrorStream());
    return GLYPHS_NEEDED;
  }

  return fontIndex;
}


RGBCode getRGB(unsigned short currentStyle, unsigned char shiftToWhichRGBRegion)
{
  Uint8 rgbMask = WHITE;
  RGBCode rgbCode;

  rgbMask &= currentStyle >> shiftToWhichRGBRegion;
  rgbCode.r = RED;
  rgbCode.r &= rgbMask;
  rgbCode.g = GREEN;
  rgbCode.g &= rgbMask;
  rgbCode.b = BLUE;
  rgbCode.b &= rgbMask;

  return rgbCode;
}

unsigned char getStyle(unsigned char shiftToWhichStyleRegion, unsigned short currentStyle)
{
  unsigned char toggle = 1;

  toggle &= (currentStyle >> shiftToWhichStyleRegion);

  return toggle;
}


HeldGraphicAndStyle *getHeldGraphic()
{
  static HeldGraphicAndStyle heldGraphicAndStyle;

  return &heldGraphicAndStyle;
}


void setHeldGraphic(unsigned char currentGraphic, unsigned char sepGfxToggle)
{
  HeldGraphicAndStyle *heldStyle = getHeldGraphic();

  heldStyle->lastGraphic = currentGraphic;
  heldStyle->lastSepState = sepGfxToggle;
}


void copyGlyphTextureToRenderer(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char glyphToBeDrawn, unsigned short currentStyle, unsigned char shiftToWhichRGBRegion,
    SDL_Rect srcRect, SDL_Rect rectToDrawTo)
{
  RGBCode rgbCode;

  rgbCode = getRGB(currentStyle, shiftToWhichRGBRegion);
  SDL_SetTextureColorMod(imageFont[glyphToBeDrawn], (Uint8)(rgbCode.r * FULL_RGB),
    (Uint8)(rgbCode.g * FULL_RGB), (Uint8)(rgbCode.b * FULL_RGB));
  SDL_RenderCopy(sw->renderer, imageFont[glyphToBeDrawn], &srcRect, &rectToDrawTo);
}


void drawCellFG(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char currentHexCode, unsigned short currentStyle, unsigned short aboveStyle,
    SDL_Rect srcRect, SDL_Rect rectToDrawTo)
{
  unsigned char glyphToBeDrawn, shiftToWhichRGBRegion = SHIFT_TO_A_RGB_REGION,
      doubleHeightToggle = getStyle(SHIFT_TO_D_HEIGHT_REGION, currentStyle),
      doubleHeightAboveToggle = getStyle(SHIFT_TO_D_HEIGHT_REGION, aboveStyle),
      gfxToggle = getStyle(SHIFT_TO_GFX_MODE_REGION, currentStyle),
      sepGfxToggle = getStyle(SHIFT_TO_SEP_GFX_REGION, currentStyle),
      holdGfxToggle = getStyle(SHIFT_TO_HOLD_GFX_REGION, currentStyle),
      clearHeldGfxToggle = getStyle(SHIFT_TO_CLEAR_HELD_GFX_REGION, currentStyle);
  HeldGraphicAndStyle *heldGlyphAndStyle;

  /* If hold gfx: use the retained glyph, with its sep/config state, as the glyphToBeDrawn.
   * This is in contrast to Neill's implementation shown in test.m7, wherein hold
   * gfx seems to always be contiguous. */
  if(holdGfxToggle){
    /* When the clearHeldGfxToggle signal is received, reset that graphic to a space,
     * but retain the present sepGfxToggle. */
    if(clearHeldGfxToggle){
      setHeldGraphic(FONT_INDEX_FOR_SPACE, sepGfxToggle);
    }

    heldGlyphAndStyle = getHeldGraphic();
    glyphToBeDrawn = heldGlyphAndStyle->lastGraphic;
    sepGfxToggle = heldGlyphAndStyle->lastSepState;
  }
  else{
    /* If release gfx: take the new glyphToBeDrawn and retain it, along with its
     * sep/config state. */
    glyphToBeDrawn = translateHexCodeToFontIndex(currentHexCode, gfxToggle);
    setHeldGraphic(glyphToBeDrawn, sepGfxToggle);
  }

  if(doubleHeightToggle){
    srcRect.h = GLYPH_HEIGHT / DENOMINATOR_FOR_HALF;
    if(doubleHeightAboveToggle){
      srcRect.y += srcRect.h;
    }
  }

  if(gfxToggle){
    shiftToWhichRGBRegion = SHIFT_TO_GFX_RGB_REGION;
  }

  /* Gets the background rgbCode by default; if in drawFG mode, gets the gfx_rgb
   * or a_rgb code. */
  copyGlyphTextureToRenderer(sw, imageFont, glyphToBeDrawn, currentStyle,
      shiftToWhichRGBRegion, srcRect, rectToDrawTo);
  /* Separating grid is overlayed when toggled on, but only during gfxmode. */
  if(gfxToggle && sepGfxToggle){
    copyGlyphTextureToRenderer(sw, imageFont, SEPARATING_GRID_INDEX, currentStyle,
        SHIFT_TO_BG_RGB_REGION, srcRect, rectToDrawTo);
  }
}


int traverseAndDrawTeletextPage(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char hexStringAsArray[ROWS][COLUMNS], unsigned short styles[ROWS][COLUMNS])
{
  unsigned int i = 0, currentColumn = 0, currentRow = 0;
  unsigned char currentHexCode;
  unsigned short currentStyle, aboveStyle = 0;
  SDL_Rect rectToDrawTo, srcRect;

  srcRect.h = rectToDrawTo.h = GLYPH_HEIGHT;
  srcRect.w = rectToDrawTo.w = GLYPH_WIDTH;
  srcRect.x = srcRect.y = rectToDrawTo.x = rectToDrawTo.y = 0;

  while(i < ROWS * COLUMNS){

    currentColumn = i % COLUMNS;
    rectToDrawTo.x = currentColumn * GLYPH_WIDTH;

    currentStyle = styles[currentRow][currentColumn];
    if(currentRow > 0){
      aboveStyle = styles[currentRow - 1][currentColumn];
    }
    currentHexCode = hexStringAsArray[currentRow][currentColumn];

    copyGlyphTextureToRenderer(sw, imageFont, BACKGROUND_GLYPH_INDEX, currentStyle,
        SHIFT_TO_BG_RGB_REGION, srcRect,rectToDrawTo);
    drawCellFG(sw, imageFont, currentHexCode, currentStyle, aboveStyle, srcRect,
        rectToDrawTo);

    i++;

    if(i % COLUMNS == 0){
      currentRow++;
      rectToDrawTo.y = (currentRow * GLYPH_HEIGHT);
    }
  }

  return 0;
}


int freeImageFont(SDL_Texture* imageFont[GLYPHS_NEEDED])
{
  int i, destroyed = 0;

  for(i = 0; i < GLYPHS_NEEDED; i++){
    if(imageFont[i] != NULL){
      SDL_DestroyTexture(imageFont[i]);
      destroyed++;
    }
  }

  return destroyed;
}


char keyup(SDL_Event *event)
{
  SDL_Scancode scancode = event->key.keysym.scancode;
  char charToPrint = NULLCHAR;

  /* Note: the scancode for '0' is above 9 rather than below 1, unlike in ASCII. */
  if((scancode >= SDL_SCANCODE_1) && (scancode <= SDL_SCANCODE_9)){
    charToPrint = '1';
    charToPrint += scancode - START_OF_SDL_NUMBER_SCANCODES;
  }
  if(scancode == SDL_SCANCODE_0){
    charToPrint = '0';
  }
  if(scancode == SDL_SCANCODE_RETURN){
    charToPrint = ENTER;
  }
  if(scancode == SDL_SCANCODE_RIGHT){
    charToPrint = NEXT_PAGE;
  }
  if(scancode == SDL_SCANCODE_LEFT){
    charToPrint = PREV_PAGE;
  }

  return charToPrint;
}


int handleEvent(SDL_Event *event, char *typedNum, ExtensionOrBasic extensionOrBasic)
{
  switch(event->type){
    case SDL_KEYUP:
      if(extensionOrBasic == EXTENSION){
        *typedNum = keyup(event);
      }
      return 1;
    case SDL_QUIT:
      return 0;
    default:
      return 1;
  }
}


void renderPageToScreen(unsigned char rawPage[ROWS * COLUMNS],
    unsigned char frameBuffer[ROWS][COLUMNS], int assembledNo, unsigned short styles[ROWS][COLUMNS],
    SDL_Simplewin* sw, SDL_Texture *imageFont[GLYPHS_NEEDED])
{
  populateArrayWithHexString(rawPage, frameBuffer);
  overwriteHeader(frameBuffer, assembledNo);
  setStylesToDefault(styles);
  traversePageToGetStylesFromHexArray(frameBuffer, styles);

  traverseAndDrawTeletextPage(sw, imageFont, frameBuffer, styles);
  SDL_RenderPresent(sw->renderer);
  SDL_UpdateWindowSurface(sw->win);
}


void overwriteHeader(unsigned char frameBuffer[ROWS][COLUMNS], int assembledNo)
{
  int i;
  /* snprintf requires space to add a nullchar */
  /*char assembledNoAsStr[sizeof(char) * (THREE_DIGIT_NO + LENGTH_OF_NULLCHAR)];*/
  char assembledNoAsStr[sizeof(char) * (THREE_DIGIT_NO)];


  /*snprintf(assembledNoAsStr, sizeof(char) * (THREE_DIGIT_NO + LENGTH_OF_NULLCHAR),
  "%03d", assembledNo);*/
  sprintf(assembledNoAsStr, "%03d", assembledNo);
  /* printf("assembledNoAsStr is:[%d][%s]\n", assembledNo, assembledNoAsStr); */

  frameBuffer[0][COLUMN_OF_P] = HEXCODE_FOR_P | PARITY_BIT;;

  /*  if(!inputComplete){
   *    charToWrite = HEXCODE_FOR_DASH | PARITY_BIT;
   *  } */

  for(i = 0; i < THREE_DIGIT_NO; i++){
    frameBuffer[0][i + COLUMN_OF_MAGAZINE] = (unsigned char)(assembledNoAsStr[i] | PARITY_BIT);
    /* printf("frameBuffer[0][i] = [%c]\n", frameBuffer[0][i + COLUMN_OF_MAGAZINE] & 0x7f); */
  }
}
