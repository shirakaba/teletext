#ifndef DEMULTIPLEXER_DEMULTIPLEXER_H
#define DEMULTIPLEXER_DEMULTIPLEXER_H

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include "common.h"

#define packetLength 43
#define MISALIGN_OF_BROADCAST 42
#define COLS_IN_PAGE 40
#define MAX_FILENAME_AND_PATH_CHARS 100
#define FIRST_MAGAZINE 1
#define LAST_MAGAZINE 8
#define NON_CODING_CHARS_IN_HEADER 8
#define PACKETS_READ_PER_SECOND 30
#define BROADCAST_FILEPATH "assets/txtdata_040406/txt0.dat"
#define ONE_SECOND 1000
#define UNREADABLE_CHAR '?'

enum MagazineBitShiftCode {
  SHIFT_TO_BIT_1_OF_MAGAZINE = 1,
  SHIFT_TO_BIT_2_OF_MAGAZINE,
  SHIFT_TO_BIT_3_OF_MAGAZINE
};
typedef enum MagazineBitShiftCode MagazineBitShiftCode;

enum PacketNoBitShiftCode {
  SHIFT_TO_BIT_1_OF_PACKETNO = 7,
  SHIFT_TO_BIT_2_OF_PACKETNO = 0,
  SHIFT_TO_BIT_3_OF_PACKETNO,
  SHIFT_TO_BIT_4_OF_PACKETNO,
  SHIFT_TO_BIT_5_OF_PACKETNO
};
typedef enum PacketNoBitShiftCode PacketNoBitShiftCode;

enum PageUnitsBitShiftCode {
  SHIFT_TO_BIT_1_OF_PAGEUNITS = 1,
  SHIFT_TO_BIT_2_OF_PAGEUNITS,
  SHIFT_TO_BIT_3_OF_PAGEUNITS,
  SHIFT_TO_BIT_4_OF_PAGEUNITS
};
typedef enum PageUnitsBitShiftCode PageUnitsBitShiftCode;

enum PageTensBitShiftCode {
  SHIFT_TO_BIT_1_OF_PAGETENS = 1,
  SHIFT_TO_BIT_2_OF_PAGETENS,
  SHIFT_TO_BIT_3_OF_PAGETENS,
  SHIFT_TO_BIT_4_OF_PAGETENS
};
typedef enum PageTensBitShiftCode PageTensBitShiftCode;

enum BinaryPositions {
  BIT_1 = 0x2,
  BIT_3 = 0x8,
  BIT_5 = 0x20,
  BIT_7 = 0x80
};
typedef enum BinaryPositions BinaryPositions;

enum TTextBytePositions {
  BYTE4 = 1,
  BYTE5,
  BYTE6,
  BYTE7
};
typedef enum TTextBytePositions TTextBytePositions;

enum RewindNeeded {
  FOUND_WITHOUT_REWINDING,
  REWIND_NECESSITATED,
  CANT_FIND_IN_BROADCAST
};
typedef enum RewindNeeded RewindNeeded;


RewindNeeded getNextPacket(FILE *broadcastFilepath, unsigned char *packet);
RewindNeeded getNextPacketOfMagazine(FILE *broadcastFilepath, unsigned char *packet, int targetMagazine);
RewindNeeded getPageOfMagazine(FILE *broadcastFile, int userInput, unsigned char *rawPage);

/* void displayPacket(unsigned char *packet); */
/* long int getFileLen(FILE* file); */
RewindNeeded intToEnumForRewindNeeded(int rewindNeeded);
int getMagazine(char byte4OfPacket);
int getPacketNumber(char byte4OfPacket, char byte5OfPacket);
int getPageUnits(char byte6OfPacket);
int getPageTens(char byte7OfPacket);

int simpleAssembleNumber(char typedNum);
char getWhetherInputComplete(unsigned long int ticksAtCurrentCall);
int assembleNumber(char typedNum, char inputComplete);

#endif /*DEMULTIPLEXER_DEMULTIPLEXER_H*/
