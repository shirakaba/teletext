#ifndef MYTEST_TEST_H
#define MYTEST_TEST_H

#include <check.h>
#include "../src/common.h"
#include "../src/demux.h"

#define FULL_MASK 65535

void tfngenerateToolsForSettingStylesBit(unsigned short invertedMask,
                                         unsigned short input, ShiftToRegion bitShift, BitsInRegion bitsInRegion);
void tfnsetCell(unsigned short styles[ROWS][COLUMNS], unsigned short styleInput, unsigned short gfxModeInput, ShiftToRegion styleShift);
void tfnSetStyleBits(unsigned short styles[ROWS][COLUMNS], unsigned short styleInput, ShiftToRegion styleShift);
void tfnGetRGB(unsigned short inputColour, ShiftToRegion styleShift);
void tfnTraversePageToGetStylesFromHexArray(int i, int limit, int row, unsigned short styles[ROWS][COLUMNS], unsigned short targetCode);
void setupSDL(void);
void teardownSDL(void);
Suite *makeMasterSuite(void);
Suite *makeSuiteForLoadIn(void);
Suite *makeSuiteThatUsesSDL(void);
Suite *makeSuiteForStyles(void);

#endif /*MYTEST_TEST_H*/
