/* Extension to Teletext display program: Allows one to view all the pages and subpages of a recorded Teletext broadcast
 * (expects the format provided on http://www.g7jjf.com/teletext.htm, with a manually calculated offset of 42 characters;
 * this offset value is hardcoded in for this proof-of-concept demonstration, as the information needed to calculate the
 * offset automatically has annoyingly been stripped out of the recording).
 *
 * USAGE:
 * 1) Input an entry number using the number keys located above the home row on the keyboard (not the numpad).
 *    Entries of particular interest: 145, 171, 200, 453, 555, and 562.
 * 2) Press enter to load in that page from the current seek position in the recorded broadcast stream.
 *    Pressing enter again will request the same page from the next seek position in the stream; this allows one to view
 *    sub-pages or refresh any pages where insufficient data was broadcast.
 * 3) Alternatively, press RIGHTARROW or LEFTARROW to increment/decrement the page number.
 *
 * FAQ:
 * - WHAT DO THE THREE DIGITS OF THE ENTRY REPRESENT?
 *    The most significant digit is the magazine number; the lesser two are the page's number.
 * - WHY DOESN'T DOUBLE-HEIGHT WORK?
 *    Height is determined by Neill's specification, not the real Teletext specification.
 * - PAGES DISPLAY AS QUESTION MARKS:
 *    These pages weren't found within the recorded broadcast (see below for more).
 * - CERTAIN ENTRIES AREN'T VIEWABLE:
 *    By specification, the viewable entries are only between 100-899. Furthermore, only a certain number of pages were
 *    transmitted during the recording made of the broadcast.
 * - THE TIMESTAMP IS WRONG:
 *    The timestamp is actually encoded by the broadcast stream, and is a useful indicator of which seek position in the
 *    broadcast recording the user has loaded.
 * - PAGES CHANGE WHEN REFRESHED:
 *    ... Just like in real Teletext! By fetching the same page at a later point in the broadcast, sub-pages can be
 *    viewed, and updating pages like stock tickers can do so.
 * - SUBPAGES DON'T ALWAYS RETURN TO THEIR ORIGINAL NUMBER UPON REFRESH:
 *    The broadcast recording is not a perfect loop.
 * - IT'S NOT LIKE REAL TELETEXT:
 *    See below.
 *
 * EXTRA FEATURES ATTEMPTED:
 * I tried for a long time to match two of the behaviours of real Teletext:
 * 1) constantly reading the latest packets off a broadcast at the real rate of 30 per second.
 * 2) typed entry numbers becoming either appended to a previous one, or finalised after one second of grace time.
 * ... Unfortunately, after much effort, I had to abandon these features as I couldn't find a way to slow down the event-
 * handling loop without completely breaking feature 2). Part of the problem lies with the fact that they should really
 * exist as functions on two independent threads, and I don't have time to investigate multi-threading. The functions
 * and tests for 2) are still in the code (getWhetherInputComplete() and assembleNumber() ), while much of the logic for
 * feature 1) is already set up but not being put to use.
 * */

#include "teletext.h"
#include "demux.h"


int main()
{
  int continuing = 1, result, assembledNo = LANDING_PAGE;
  char typedNum = NULLCHAR;
  unsigned char rawPage[ROWS * COLUMNS], frameBuffer[ROWS][COLUMNS];
  unsigned short styles[ROWS][COLUMNS];
  FILE *broadcastFile = fopen(BROADCAST_FILEPATH, "rb");
  SDL_Simplewin sw;
  SDL_Texture* imageFont[GLYPHS_NEEDED];
  SDL_Event event;

  setErrorStream(stderr);
  fseek(broadcastFile, MISALIGN_OF_BROADCAST, SEEK_SET);
  memset(rawPage, UNREADABLE_CHAR, ROWS * COLUMNS);

  /* NOTE: Needs SDL to be initialised before populating imageFont because SDL is
   * used to generate the textures to be passed in. */
  result = SDL_InitWrapper(&sw);
  exitIfIntTestAboveZero(result);
  result = populateImageFont(imageFont, &sw);
  exitIfIntTestAboveZero(result);

  getPageOfMagazine(broadcastFile, assembledNo, rawPage);

  renderPageToScreen(rawPage, frameBuffer, assembledNo, styles, &sw, imageFont);

  while(continuing){
    while (SDL_PollEvent(&event)) {
      continuing = handleEvent(&event, &typedNum, EXTENSION);
      if(typedNum != NULLCHAR) {
        switch(typedNum){
          case NEXT_PAGE:
            assembledNo = (assembledNo + 1) % MAX_TELETEXT_PAGES;
            getPageOfMagazine(broadcastFile, assembledNo, rawPage);
            break;
          case PREV_PAGE:
            if(assembledNo == 0){
              assembledNo = MAX_TELETEXT_PAGES - 1;
            }
            else{
              assembledNo = (assembledNo - 1) % MAX_TELETEXT_PAGES;
            }
            getPageOfMagazine(broadcastFile, assembledNo, rawPage);
            break;
          case ENTER:
            getPageOfMagazine(broadcastFile, assembledNo, rawPage);
            break;
          default:
            assembledNo = simpleAssembleNumber(typedNum);
        }
      }

      renderPageToScreen(rawPage, frameBuffer, assembledNo, styles, &sw, imageFont);

      typedNum = NULLCHAR;
    }
  }

  fclose(broadcastFile);
  freeImageFont(imageFont);
  SDL_Quit();

  return 0;
}
