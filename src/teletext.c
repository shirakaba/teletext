/* Teletext display program.
 * Images to construct font were sourced from http://8bs.com/submit/bfont.htm.
 * The makefile relies on headers and includes that I provide, so there may be potential for a systems update to break something.
 *
 * ERRATA:
 * 1) To match Neill's implementation of separated graphics in test.m7, all control codes concerning graphics
 * transition you into gfx mode, avoiding the display of an alpha character after the word 'Separated'.
 *
 * 2) The pound sterling sign is produced by code 0xA3, as required in the assignment specification (although unlike the '#' that
 * test.m7 expects).
 *
 * 3) The separated/contiguous mode in my implementation is preserved during hold graphics, unlike what test.m7 expects;
 * there is no specific guidance on it in the specification, so I went with this because I believe it gives more functionality.
 * */

#include "teletext.h"


int main(int argc, char *argv[])
{
  int result, continuing = 1;
  void *presult;
  char *hexFileFilepath = argv[ARGC_FOR_FILEPATH], typedNum;
  unsigned char rawPage[ROWS * COLUMNS], frameBuffer[ROWS][COLUMNS];
  unsigned short styles[ROWS][COLUMNS];
  FILE *pHexFileFilepath = NULL;
  SDL_Simplewin sw;
  SDL_Texture* imageFont[GLYPHS_NEEDED];
  SDL_Event event;

  setErrorStream(stderr);
  result = validateArgCount(argc, ARGS_NEEDED);
  exitIfIntTestAboveZero(result);

  /* NOTE: Needs SDL to be initialised before populating imageFont because SDL is used to generate the textures to be passed in. */
  result = SDL_InitWrapper(&sw);
  exitIfIntTestAboveZero(result);
  result = populateImageFont(imageFont, &sw);
  exitIfIntTestAboveZero(result);

  presult = pHexFileFilepath = validateHexFile(hexFileFilepath);
  exitIfPointerTestFailed(presult);
  presult = readHexFileIntoString(rawPage, pHexFileFilepath);
  exitIfPointerTestFailed(presult);

  populateArrayWithHexString(rawPage, frameBuffer);
  setStylesToDefault(styles);
  traversePageToGetStylesFromHexArray(frameBuffer, styles);

  traverseAndDrawTeletextPage(&sw, imageFont, frameBuffer, styles);
  SDL_RenderPresent(sw.renderer);
  SDL_UpdateWindowSurface(sw.win);

  while(continuing){
    while (SDL_PollEvent(&event)) {
      continuing = handleEvent(&event, &typedNum, BASIC);
    }
  }

  freeImageFont(imageFont);
  SDL_Quit();

  return 0;
}
