#include "test.h"

#define VALID_HEXFILE "assets/test.m7"

/* Note: getErrorStream() and setErrorStream() are functionally tested already.
 * This program simultaneously tests both the Teletext program and its extension program.*/

START_TEST(test_validateArgCount){

    int argc, ARGS_NEEDED = 2;

    argc = ARGS_NEEDED;
    ck_assert( validateArgCount(argc, ARGS_NEEDED) == 0);

    argc = ARGS_NEEDED + 1;
    ck_assert( validateArgCount(argc, ARGS_NEEDED) == 1);

    argc = ARGS_NEEDED - 1;
    ck_assert( validateArgCount(argc, ARGS_NEEDED) == 1);

  }
END_TEST


START_TEST(test_validateHexFile){

    char *filepath = NULL;
    FILE *returnedFp = NULL;

    filepath = "incorrectFilepath";
    returnedFp = validateHexFile(filepath);
    ck_assert(returnedFp == NULL);

    filepath = VALID_HEXFILE;
    returnedFp = validateHexFile(filepath);
    ck_assert(returnedFp != NULL);
    fclose(returnedFp);

}
END_TEST


/* Assumption made that VALID_HEXFILE is indeed valid and openable (depends on results of test_validateHexFile) */
START_TEST(test_readHexFileIntoString){

    FILE *file  = NULL;
    unsigned char rawPage[ROWS * COLUMNS];

    file = fopen(VALID_HEXFILE, "rb");
    ck_assert(readHexFileIntoString(rawPage, file) != NULL);

  }
END_TEST


/* Could add a few more tests. */
START_TEST(test_populateArrayWithHexString){

    unsigned char array[ROWS][COLUMNS], hexString[] = "ABC";
    unsigned int A_WITH_PARITY = 193, i = 0, j = 0;

    while(i < (ROWS * COLUMNS)){
      array[j][i % COLUMNS] = 0;
      i++;

      if(i % COLUMNS == 0){
        j++;
      }
    };

    populateArrayWithHexString(hexString, array);
    ck_assert(array[0][0] == A_WITH_PARITY);
    ck_assert(array[0][1] == A_WITH_PARITY + 'B' - 'A');
    ck_assert(array[0][2] == A_WITH_PARITY + 'C' - 'A');
  }
END_TEST


/* */
START_TEST(test_getCodesFromHexArray){

    /* Assumed to be working */
    FILE *validFile = fopen(VALID_HEXFILE, "rb");
    unsigned char rawPage[ROWS * COLUMNS], *presult, hexStringAsArray[ROWS][COLUMNS];

    presult = readHexFileIntoString(rawPage, validFile);
    exitIfPointerTestFailed(presult);

    populateArrayWithHexString(rawPage, hexStringAsArray);
  }
END_TEST


/* NOTE: Uncharacterised behaviour if digitsOfID < i. */
START_TEST(test_generateStrFromInt){
    int result;
    char* generatedStr;

    generatedStr = generateStrFromInt(53, 3, IMG);
    result = strcmp(generatedStr, "053.gif");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(53, 3, HEXFILE);
    result = strcmp(generatedStr, "053.m7");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(53, 5, IMG);
    result = strcmp(generatedStr, "00053.gif");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(53, 5, HEXFILE);
    result = strcmp(generatedStr, "00053.m7");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(53, 2, IMG);
    result = strcmp(generatedStr, "53.gif");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(5, 1, IMG);
    result = strcmp(generatedStr, "5.gif");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateStrFromInt(5, 1, HEXFILE);
    result = strcmp(generatedStr, "5.m7");
    ck_assert(result == 0);
    free(generatedStr);
  }
END_TEST


/* NOTE: Uncharacterised behaviour if i exceeds 9 digits. */
START_TEST(test_generateFilePath){
    int result;
    char* generatedStr;

    generatedStr = generateFilePath(53, "assets/gfx/", IMG);
    ck_assert(generatedStr != NULL);
    result = strcmp(generatedStr, "assets/gfx/053.gif");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateFilePath(53, "assets/gfx/", IMG);
    ck_assert(generatedStr != NULL);
    result = strcmp(generatedStr, "assets/gfx/053.gif0");
    ck_assert(result != 0);
    free(generatedStr);

    generatedStr = generateFilePath(53, "assets/", HEXFILE);
    ck_assert(generatedStr != NULL);
    result = strcmp(generatedStr, "assets/053.m7");
    ck_assert(result == 0);
    free(generatedStr);

    generatedStr = generateFilePath(53, "assets/", HEXFILE);
    ck_assert(generatedStr != NULL);
    result = strcmp(generatedStr, "assets/053.m70");
    ck_assert(result != 0);
    free(generatedStr);
  }
END_TEST


START_TEST(test_translateHexCodeToFontIndex){

    /* GLYPHS_NEEDED is the error code, as it is just out of the range of the imageFont. */

    ck_assert(translateHexCodeToFontIndex(0x01, 0) == FONT_INDEX_FOR_SPACE);
    ck_assert(translateHexCodeToFontIndex(0xa0, 0) == 0);
    ck_assert(translateHexCodeToFontIndex(0xff, 0) == TOTAL_GLYPHS_IN_MODE - 1);
    ck_assert(translateHexCodeToFontIndex(0xa0, 1) == 0);
    ck_assert(translateHexCodeToFontIndex(0xa1, 1) == TOTAL_GLYPHS_IN_MODE + 1);
    ck_assert(translateHexCodeToFontIndex(0xbf, 1) == TOTAL_GLYPHS_IN_MODE + 31);
    ck_assert(translateHexCodeToFontIndex(0xc0, 1) == 32);
    ck_assert(translateHexCodeToFontIndex(0xdf, 1) == 63);
    ck_assert(translateHexCodeToFontIndex(0xe0, 1) == 128);
    ck_assert(translateHexCodeToFontIndex(0xff, 1) == GLYPHS_NEEDED - 1);
  }
END_TEST


/* ================================================= */
/* ============ START OF SDL TEST SUITE ============ */
/* ================================================= */


/* */
START_TEST(test_SDL_InitWrapper){
    SDL_Simplewin sw;

    ck_assert(SDL_InitWrapper(&sw) == 0);
  }
END_TEST


/* Stress-testing of this function is overkill as it would require changing the number of assets to conflict with the hashdefines. */
START_TEST(test_loadGlyphsFromFolderIntoImageFont){
    SDL_Simplewin sw;
    SDL_Texture* imageFont[GLYPHS_NEEDED];
    unsigned int arrayPosToLoadInto = 0;
    int result;

    result = SDL_InitWrapper(&sw);
    exitIfIntTestAboveZero(result);

    arrayPosToLoadInto = loadGlyphsFromFolderIntoImageFont(imageFont, &sw, 0, LOAD_FROM_ALPHA_FOLDER);
    ck_assert(arrayPosToLoadInto == RESUMED_GFX_GLYPH_FILENUMBER);

    arrayPosToLoadInto = loadGlyphsFromFolderIntoImageFont(imageFont, &sw, arrayPosToLoadInto, LOAD_FROM_GFX_FOLDER);
    ck_assert(arrayPosToLoadInto == GLYPHS_NEEDED);
  }
END_TEST


/* */
START_TEST(test_populateImageFont){
    SDL_Simplewin sw;
    SDL_Texture* imageFont[GLYPHS_NEEDED];
    int result;

    result = SDL_InitWrapper(&sw);
    exitIfIntTestAboveZero(result);

    result = populateImageFont(imageFont, &sw);
    ck_assert(result == 0);
  }
END_TEST


/* Stress-testing of this function is overkill as it would require changing the number of assets to conflict with the hashdefines. */
START_TEST(test_IMG_LoadToSurface){
    SDL_Simplewin sw;
    SDL_Surface *surface;
    int result;
    void* presult;

    result = SDL_InitWrapper(&sw);
    exitIfIntTestAboveZero(result);

    presult = surface = IMG_LoadToSurface("assets/imgfnt/gfx/053.gif", DONT_SET_WHITE_AS_ALPHA);
    exitIfPointerTestFailed(presult);
    SDL_FreeSurface(surface);

    ck_assert(result == 0);
  }
END_TEST


/* Stress-testing of this function is overkill as it would require changing the number of assets to conflict with the hashdefines. */
START_TEST(test_surfaceToTexture){
    SDL_Simplewin sw;
    SDL_Surface* surface;
    int result;
    void* presult;

    result = SDL_InitWrapper(&sw);
    exitIfIntTestAboveZero(result);

    presult = surface = IMG_LoadToSurface("assets/imgfnt/gfx/053.gif", DONT_SET_WHITE_AS_ALPHA);
    exitIfPointerTestFailed(presult);

    presult = surfaceToTexture(&sw, surface);
    exitIfPointerTestFailed(presult);

    ck_assert(presult != NULL);
  }
END_TEST


START_TEST(test_freeImageFont){
    SDL_Simplewin sw;
    SDL_Texture* imageFont[GLYPHS_NEEDED];
    unsigned int arrayPosToLoadInto = 0;
    int result;

    result = SDL_InitWrapper(&sw);
    exitIfIntTestAboveZero(result);

    arrayPosToLoadInto = loadGlyphsFromFolderIntoImageFont(imageFont, &sw, 0, LOAD_FROM_ALPHA_FOLDER);
    ck_assert(arrayPosToLoadInto == RESUMED_GFX_GLYPH_FILENUMBER);

    arrayPosToLoadInto = loadGlyphsFromFolderIntoImageFont(imageFont, &sw, arrayPosToLoadInto, LOAD_FROM_GFX_FOLDER);
    ck_assert(arrayPosToLoadInto == GLYPHS_NEEDED);

    ck_assert(freeImageFont(imageFont) > 0);
    ck_assert(freeImageFont(imageFont) == GLYPHS_NEEDED);
  }
END_TEST

/* A function only used for the extension. */
START_TEST(test_keyup){
    SDL_Event event;
    SDL_Init(SDL_INIT_VIDEO);

    event.type = SDL_KEYUP;
    event.key.type = SDL_KEYUP;
    event.key.state = SDL_PRESSED;

    event.key.keysym.scancode = SDL_SCANCODE_0;
    ck_assert(keyup(&event) == '0');

    event.key.keysym.scancode = SDL_SCANCODE_1;
    ck_assert(keyup(&event) == '1');

    event.key.keysym.scancode = SDL_SCANCODE_9;
    ck_assert(keyup(&event) == '9');

    event.key.keysym.scancode = SDL_SCANCODE_RETURN;
    ck_assert(keyup(&event) == ENTER);

    event.key.keysym.scancode = SDL_SCANCODE_BACKSPACE;
    ck_assert(keyup(&event) == NULLCHAR);
  }
END_TEST


/* A function mostly used in the extension. */
START_TEST(test_handleEvent){
    SDL_Event event;
    char typedNum;
    SDL_Init(SDL_INIT_VIDEO);

    event.type = SDL_KEYUP;
    event.key.type = SDL_KEYUP;
    event.key.state = SDL_PRESSED;

    event.key.keysym.scancode = SDL_SCANCODE_0;
    ck_assert(handleEvent(&event, &typedNum, EXTENSION) == 1);
    ck_assert(typedNum == '0');

    event.key.keysym.scancode = SDL_SCANCODE_1;
    ck_assert(handleEvent(&event, &typedNum, EXTENSION) == 1);
    ck_assert(typedNum == '1');

    ck_assert(handleEvent(&event, &typedNum, BASIC) == 1);

    event.key.type = 0;
    event.key.state = 0;
    event.type = SDL_QUIT;
    event.quit.type = SDL_QUIT;

    ck_assert(handleEvent(&event, &typedNum, EXTENSION) == 0);
    ck_assert(handleEvent(&event, &typedNum, BASIC) == 0);
  }
END_TEST



/* ================================================== */
/* ============ START OF STYLE TEST SUITE =========== */
/* ================================================== */


/*  */
START_TEST(test_setStylesToDefault){
    int currentColumn = 0, currentRow = 0;
    unsigned short styles[ROWS][COLUMNS];

    setStylesToDefault(styles);

    while(currentColumn < ROWS * COLUMNS){
      ck_assert(styles[currentRow][currentColumn % COLUMNS] == DEFAULT_CODE);
      currentColumn++;

      if(currentColumn % COLUMNS == 0){
        currentRow++;
      }
    };

}
END_TEST


START_TEST(test_convertNoOfBitsToBitPattern){
    unsigned char radix = 2;

    /* 0b111 is 7*/
    ck_assert(convertNoOfBitsToBitPattern(RGB_REGION, radix) == 7);
    ck_assert(convertNoOfBitsToBitPattern(SINGLE_BIT_REGION, radix) == 1);
    ck_assert(convertNoOfBitsToBitPattern(NULL_REGION, radix) == 0);

  }
END_TEST


START_TEST(test_getRGBBitPattern){
    ck_assert(getRGBBitPattern((WHITE << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (WHITE << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((RED << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (RED << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((GREEN << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (GREEN << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((BLUE << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (BLUE << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((CYAN << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (CYAN << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((MAGENTA << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (MAGENTA << SHIFT_TO_A_RGB_REGION));
    ck_assert(getRGBBitPattern((YELLOW << SHIFT_TO_A_RGB_REGION), SHIFT_TO_A_RGB_REGION) == (YELLOW << SHIFT_TO_A_RGB_REGION));

    /* Not actually using these functionalities in the program, so just a couple of tests: */
    ck_assert(getRGBBitPattern((WHITE << SHIFT_TO_GFX_RGB_REGION), SHIFT_TO_GFX_RGB_REGION) == (WHITE << SHIFT_TO_GFX_RGB_REGION));
    ck_assert(getRGBBitPattern((WHITE << SHIFT_TO_BG_RGB_REGION), SHIFT_TO_BG_RGB_REGION) == (WHITE << SHIFT_TO_BG_RGB_REGION));
  }
END_TEST


START_TEST(test_getControlCodeInput){
    ck_assert(getControlCodeInput(A_RED) == RED);
    ck_assert(getControlCodeInput(GFX_RED) == RED);

    ck_assert(getControlCodeInput(A_GREEN) == GREEN);
    ck_assert(getControlCodeInput(GFX_GREEN) == GREEN);

    ck_assert(getControlCodeInput(A_YELLOW) == YELLOW);
    ck_assert(getControlCodeInput(GFX_YELLOW) == YELLOW);

    ck_assert(getControlCodeInput(A_BLUE) == BLUE);
    ck_assert(getControlCodeInput(GFX_BLUE) == BLUE);

    ck_assert(getControlCodeInput(A_MAGENTA) == MAGENTA);
    ck_assert(getControlCodeInput(GFX_MAGENTA) == MAGENTA);

    ck_assert(getControlCodeInput(A_CYAN) == CYAN);
    ck_assert(getControlCodeInput(GFX_CYAN) == CYAN);

    ck_assert(getControlCodeInput(A_WHITE) == WHITE);
    ck_assert(getControlCodeInput(GFX_WHITE) == WHITE);

    ck_assert(getControlCodeInput(BG_BLACK) == BLACK);
    ck_assert(getControlCodeInput(BG_NEW) == NEW_BG_REQUESTED);

    ck_assert(getControlCodeInput(DOUBLE_HEIGHT) == TOGGLE_ON);
    ck_assert(getControlCodeInput(SEP_GFX) == TOGGLE_ON);
    ck_assert(getControlCodeInput(HOLD_GFX) == TOGGLE_ON);

    ck_assert(getControlCodeInput(SINGLE_HEIGHT) == TOGGLE_OFF);
    ck_assert(getControlCodeInput(CONTIG_GFX) == TOGGLE_OFF);
    ck_assert(getControlCodeInput(RELEASE_GFX) == TOGGLE_OFF);

    ck_assert(getControlCodeInput(128) == NULL_INPUT);
  }
END_TEST


START_TEST(test_getBitsInRegion){
    ck_assert(getBitsInRegion(SINGLE_HEIGHT) == SINGLE_BIT_REGION);
    ck_assert(getBitsInRegion(DOUBLE_HEIGHT) == SINGLE_BIT_REGION);
    ck_assert(getBitsInRegion(CONTIG_GFX) == SINGLE_BIT_REGION);
    ck_assert(getBitsInRegion(SEP_GFX) == SINGLE_BIT_REGION);
    ck_assert(getBitsInRegion(HOLD_GFX) == SINGLE_BIT_REGION);
    ck_assert(getBitsInRegion(RELEASE_GFX) == SINGLE_BIT_REGION);

    ck_assert(getBitsInRegion(A_RED) == RGB_REGION);
    ck_assert(getBitsInRegion(A_GREEN) == RGB_REGION);
    ck_assert(getBitsInRegion(A_YELLOW) == RGB_REGION);
    ck_assert(getBitsInRegion(A_BLUE) == RGB_REGION);
    ck_assert(getBitsInRegion(A_MAGENTA) == RGB_REGION);
    ck_assert(getBitsInRegion(A_CYAN) == RGB_REGION);
    ck_assert(getBitsInRegion(A_WHITE) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_RED) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_GREEN) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_YELLOW) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_BLUE) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_MAGENTA) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_CYAN) == RGB_REGION);
    ck_assert(getBitsInRegion(GFX_WHITE) == RGB_REGION);
    ck_assert(getBitsInRegion(BG_BLACK) == RGB_REGION);
    ck_assert(getBitsInRegion(BG_NEW) == RGB_REGION);

    ck_assert(getBitsInRegion(0x20) == NULL_REGION);
  }
END_TEST


START_TEST(test_getRegionToShiftTo){
    ck_assert(getRegionToShiftTo(A_RED) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_GREEN) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_YELLOW) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_BLUE) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_MAGENTA) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_CYAN) == SHIFT_TO_A_RGB_REGION);
    ck_assert(getRegionToShiftTo(A_WHITE) == SHIFT_TO_A_RGB_REGION);

    ck_assert(getRegionToShiftTo(DOUBLE_HEIGHT) == SHIFT_TO_D_HEIGHT_REGION);
    ck_assert(getRegionToShiftTo(SINGLE_HEIGHT) == SHIFT_TO_D_HEIGHT_REGION);

    ck_assert(getRegionToShiftTo(GFX_RED) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_GREEN) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_YELLOW) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_BLUE) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_MAGENTA) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_CYAN) == SHIFT_TO_GFX_RGB_REGION);
    ck_assert(getRegionToShiftTo(GFX_WHITE) == SHIFT_TO_GFX_RGB_REGION);

    ck_assert(getRegionToShiftTo(SEP_GFX) == SHIFT_TO_SEP_GFX_REGION);
    ck_assert(getRegionToShiftTo(CONTIG_GFX) == SHIFT_TO_SEP_GFX_REGION);

    ck_assert(getRegionToShiftTo(BG_BLACK) == SHIFT_TO_BG_RGB_REGION);
    ck_assert(getRegionToShiftTo(BG_NEW) == SHIFT_TO_BG_RGB_REGION);

    ck_assert(getRegionToShiftTo(HOLD_GFX) == SHIFT_TO_HOLD_GFX_REGION);
    ck_assert(getRegionToShiftTo(RELEASE_GFX) == SHIFT_TO_HOLD_GFX_REGION);

    ck_assert(getRegionToShiftTo(0x20) == SHIFT_TO_NULL_REGION);
  }
END_TEST


void tfngenerateToolsForSettingStylesBit(unsigned short invertedMask, unsigned short input, ShiftToRegion bitShift, BitsInRegion bitsInRegion)
{
    SetBitTools setBitTools;

    setBitTools = generateToolsForSettingStylesBit(input, bitsInRegion, bitShift);
    ck_assert(setBitTools.inputBitMask == FULL_MASK - (invertedMask << bitShift));
    ck_assert(setBitTools.inputShifted == (input << bitShift));
}


START_TEST(test_generateToolsForSettingStylesBit){
    /* DEFAULT_CODE in binary is 0b000 0000 0111 0111.
     * FULL_MASK in binary is 0b1111 1111 1111 1111.*/
    unsigned short input, invertedMask;
    ShiftToRegion bitShift;
    BitsInRegion bitsInRegion;

    invertedMask = 1;
    bitsInRegion = SINGLE_BIT_REGION;

    input = 0;
    bitShift = SHIFT_TO_D_HEIGHT_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 1;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 0;
    bitShift = SHIFT_TO_SEP_GFX_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 1;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 0;
    bitShift = SHIFT_TO_HOLD_GFX_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 1;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 0;
    bitShift = SHIFT_TO_CLEAR_HELD_GFX_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 1;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 0;
    bitShift = SHIFT_TO_GFX_MODE_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = 1;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);


    invertedMask = WHITE;
    bitsInRegion = RGB_REGION;
    input = CYAN;
    bitShift = SHIFT_TO_A_RGB_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = WHITE;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = BLACK;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = YELLOW;
    bitShift = SHIFT_TO_GFX_RGB_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);

    input = MAGENTA;
    bitShift = SHIFT_TO_BG_RGB_REGION;
    tfngenerateToolsForSettingStylesBit(invertedMask, input, bitShift, bitsInRegion);
  }
END_TEST


void tfnsetCell(unsigned short styles[ROWS][COLUMNS], unsigned short styleInput, unsigned short gfxModeInput, ShiftToRegion styleShift)
{
  SetBitTools toolsForSettingBitsOfInput, toolsForSettingBitsOfAlphaOrGfxMode;

  toolsForSettingBitsOfInput = generateToolsForSettingStylesBit(styleInput, RGB_REGION, styleShift);
  toolsForSettingBitsOfAlphaOrGfxMode = generateToolsForSettingStylesBit(gfxModeInput, SINGLE_BIT_REGION, SHIFT_TO_GFX_MODE_REGION);
  setCell(styles, 0, 0, toolsForSettingBitsOfInput, toolsForSettingBitsOfAlphaOrGfxMode);
  ck_assert((styleInput & (styles[0][0] >> styleShift)) == styleInput);
  ck_assert((gfxModeInput & (styles[0][0] >> SHIFT_TO_GFX_MODE_REGION)) == gfxModeInput);
}


/* Assumes that generateToolsForSettingStylesBit() is already working. */
START_TEST(test_setCell){
    unsigned short styles[ROWS][COLUMNS], styleInput, gfxModeInput;
    ShiftToRegion styleShift;

    setStylesToDefault(styles);

    styleInput = CYAN;
    styleShift = SHIFT_TO_A_RGB_REGION;
    gfxModeInput = 1;
    tfnsetCell(styles, styleInput, gfxModeInput, styleShift);

    styleInput = WHITE;
    styleShift = SHIFT_TO_GFX_RGB_REGION;
    gfxModeInput = 1;
    tfnsetCell(styles, styleInput, gfxModeInput, styleShift);

    styleInput = BLACK;
    styleShift = SHIFT_TO_BG_RGB_REGION;
    gfxModeInput = 1;
    tfnsetCell(styles, styleInput, gfxModeInput, styleShift);

  }
END_TEST


/* Assumes that setCell() and getStyleFromHexCode() are already working. */
START_TEST(test_setRow){
    unsigned short styles[ROWS][COLUMNS];
    unsigned char failTest = 0;
    int currentColumn = 0, currentRow = 0, i = 1;
    StyleFromHexCode styleFromHexCode;

    setStylesToDefault(styles);

    styleFromHexCode = getStyleFromHexCode(A_RED, styles[currentRow][currentColumn]);
    setRow(styles, currentRow, currentColumn, styleFromHexCode);

    while(i < COLUMNS){
      if(styles[currentRow][i] != styles[currentRow][i -1]){
        failTest = 1;
      }
      i++;
    };
    ck_assert(failTest == 0);

  }
END_TEST


void tfnSetStyleBits(unsigned short styles[ROWS][COLUMNS], unsigned short styleInput, ShiftToRegion styleShift)
{
  setStyleBits(styles, 0, 0, (unsigned short)(FULL_MASK - styleInput), (styleInput << styleShift));
  ck_assert((styleInput & (styles[0][0] >> styleShift)) == styleInput);
}


/* Assumes that setCell() is already working. */
START_TEST(test_setStyleBits){
    unsigned short styles[ROWS][COLUMNS], styleInput;
    ShiftToRegion styleShift;

    styles[0][0] = 0;
    styleShift = SHIFT_TO_A_RGB_REGION;
    styleInput = RED;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = WHITE;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = BLACK;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_GFX_RGB_REGION;
    styleInput = YELLOW;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = BLACK;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = WHITE;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_BG_RGB_REGION;
    styleInput = BLUE;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = BLACK;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = WHITE;
    tfnSetStyleBits(styles, styleInput, styleShift);


    styleShift = SHIFT_TO_HOLD_GFX_REGION;
    styleInput = TOGGLE_ON;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = TOGGLE_OFF;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_D_HEIGHT_REGION;
    styleInput = TOGGLE_ON;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = TOGGLE_OFF;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_CLEAR_HELD_GFX_REGION;
    styleInput = TOGGLE_ON;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = TOGGLE_OFF;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_SEP_GFX_REGION;
    styleInput = TOGGLE_ON;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = TOGGLE_OFF;
    tfnSetStyleBits(styles, styleInput, styleShift);

    styleShift = SHIFT_TO_GFX_MODE_REGION;
    styleInput = TOGGLE_ON;
    tfnSetStyleBits(styles, styleInput, styleShift);
    styleInput = TOGGLE_OFF;
    tfnSetStyleBits(styles, styleInput, styleShift);
  }
END_TEST


/* Decided not to test functions that just hold switches, so skipping over test_getActivityOfControlCode. */

/* No dependencies. */
START_TEST(test_getStyle){
    unsigned short styles[ROWS][COLUMNS];

    setStylesToDefault(styles);

    /* RGB regions are outside of use description (multi-bit), but do work for the blue channel. */
    ck_assert(getStyle(SHIFT_TO_A_RGB_REGION, styles[0][0]) == 1);
    ck_assert(getStyle(SHIFT_TO_GFX_RGB_REGION, styles[0][0]) == 1);
    ck_assert(getStyle(SHIFT_TO_BG_RGB_REGION, styles[0][0]) == 0);

    ck_assert(getStyle(SHIFT_TO_CLEAR_HELD_GFX_REGION, styles[0][0]) == 0);
    ck_assert(getStyle(SHIFT_TO_D_HEIGHT_REGION, styles[0][0]) == 0);
    ck_assert(getStyle(SHIFT_TO_SEP_GFX_REGION, styles[0][0]) == 0);
    ck_assert(getStyle(SHIFT_TO_HOLD_GFX_REGION, styles[0][0]) == 0);
    ck_assert(getStyle(SHIFT_TO_CLEAR_HELD_GFX_REGION, styles[0][0]) == 0);
    ck_assert(getStyle(SHIFT_TO_GFX_MODE_REGION, styles[0][0]) == 0);
  }
END_TEST


void tfnGetRGB(unsigned short inputColour, ShiftToRegion styleShift)
{
    unsigned short currentStyle = (inputColour << styleShift);
    RGBCode rgbCode = getRGB(currentStyle, styleShift);

    ck_assert(rgbCode.r == (inputColour & RED));
    ck_assert(rgbCode.g == (inputColour & GREEN));
    ck_assert(rgbCode.b == (inputColour & BLUE));
}


/* No dependencies. */
START_TEST(test_getRGB){
    unsigned short inputColour;
    ShiftToRegion styleShift;

    inputColour = WHITE;
    styleShift = SHIFT_TO_A_RGB_REGION;
    tfnGetRGB(inputColour, styleShift);

    inputColour = RED;
    tfnGetRGB(inputColour, styleShift);

    inputColour = BLACK;
    tfnGetRGB(inputColour, styleShift);


    inputColour = WHITE;
    styleShift = SHIFT_TO_GFX_RGB_REGION;
    tfnGetRGB(inputColour, styleShift);

    inputColour = BLUE;
    tfnGetRGB(inputColour, styleShift);

    inputColour = BLACK;
    tfnGetRGB(inputColour, styleShift);


    inputColour = WHITE;
    styleShift = SHIFT_TO_BG_RGB_REGION;
    tfnGetRGB(inputColour, styleShift);

    inputColour = BLUE;
    tfnGetRGB(inputColour, styleShift);

    inputColour = BLACK;
    tfnGetRGB(inputColour, styleShift);
  }
END_TEST

/* Deciding not to test getIncomingGfxMode() and getIncomingHeightMode(), as they are just switches. */


START_TEST(test_getIncomingMode){
    unsigned char modePassedByCurrentHexCode, currentHexCode, currentMode;

    modePassedByCurrentHexCode = ALPHA_OR_GFX_MODE;
    currentHexCode = A_RED;
    currentMode = MODE_ENABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_DISABLED);
    currentMode = MODE_DISABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_DISABLED);
    currentHexCode = BG_BLACK;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_DISABLED);

    modePassedByCurrentHexCode = HEIGHT_MODE;
    currentHexCode = A_RED;
    currentMode = MODE_ENABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_ENABLED);
    currentMode = MODE_DISABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_DISABLED);
    currentHexCode = SINGLE_HEIGHT;
    currentMode = MODE_ENABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_DISABLED);
    currentHexCode = DOUBLE_HEIGHT;
    currentMode = MODE_DISABLED;
    ck_assert(getIncomingMode(modePassedByCurrentHexCode, currentHexCode, currentMode) == MODE_ENABLED);
    }
END_TEST


/* Assumes that getControlCodeInput(), getRGBBitPattern(), getStyle(), and getIncomingMode() are working. */
START_TEST(test_getStyleFromHexCode){
        unsigned char currentHexCode;
        unsigned short currentStyle;
        StyleFromHexCode styleFromHexCode;

        currentHexCode = A_RED;
        currentStyle = DEFAULT_CODE;
        styleFromHexCode = getStyleFromHexCode(currentHexCode, currentStyle);
        ck_assert(styleFromHexCode.input == RED);
        ck_assert(styleFromHexCode.bitsInRegion == RGB_REGION);
        ck_assert(styleFromHexCode.bitShift == SHIFT_TO_A_RGB_REGION);
        ck_assert(styleFromHexCode.alphaOrGfxMode == getIncomingMode(ALPHA_OR_GFX_MODE, currentHexCode,
                                                                     getStyle(SHIFT_TO_GFX_MODE_REGION, currentStyle)));
        ck_assert(styleFromHexCode.clearHeldGfx == 0);

        currentHexCode = GFX_GREEN;
        currentStyle = DEFAULT_CODE;
        styleFromHexCode = getStyleFromHexCode(currentHexCode, currentStyle);
        ck_assert(styleFromHexCode.input == GREEN);
        ck_assert(styleFromHexCode.bitsInRegion == RGB_REGION);
        ck_assert(styleFromHexCode.bitShift == SHIFT_TO_GFX_RGB_REGION);
        ck_assert(styleFromHexCode.alphaOrGfxMode == getIncomingMode(ALPHA_OR_GFX_MODE, currentHexCode,
                                                                     getStyle(SHIFT_TO_GFX_MODE_REGION, currentStyle)));
        ck_assert(styleFromHexCode.clearHeldGfx == 1);
  }
END_TEST


void tfnTraversePageToGetStylesFromHexArray(int i, int limit, int row, unsigned short styles[ROWS][COLUMNS], unsigned short targetCode)
{
    char failedTest = 0;

    while(i < limit){
        if(styles[row][i] != targetCode){
            failedTest = 1;
        }
        i++;
    }
    ck_assert(failedTest == 0);
}


/* Depends on getStyleFromHexCode(), generateToolsForSettingStylesBit(), setStyleBits(), getActivityOfControlCode(),
 * and setRow(), as well as the functions to read a hexFile into an array, but is ultimately just a simple 2D
 * array-traversing function calling all of them. Note: this test is designed only for use on test.m7. */
START_TEST(test_traversePageToGetStylesFromHexArray){
    void *presult;
    char *hexFileFilepath = VALID_HEXFILE;
    unsigned char rawPage[ROWS * COLUMNS], frameBuffer[ROWS][COLUMNS];
    unsigned short styles[ROWS][COLUMNS];
    FILE *pHexFileFilepath = NULL;

    presult = pHexFileFilepath = validateHexFile(hexFileFilepath);
    exitIfPointerTestFailed(presult);

    presult = readHexFileIntoString(rawPage, pHexFileFilepath);
    exitIfPointerTestFailed(presult);

    populateArrayWithHexString(rawPage, frameBuffer);

    setStylesToDefault(styles);

    traversePageToGetStylesFromHexArray(frameBuffer, styles);

    tfnTraversePageToGetStylesFromHexArray(0, COLUMNS, 0, styles, DEFAULT_CODE);
    tfnTraversePageToGetStylesFromHexArray(13, 37, 2, styles, 127);
    tfnTraversePageToGetStylesFromHexArray(13, 37, 3, styles, 127);
    tfnTraversePageToGetStylesFromHexArray(2, 11, 20, styles, 8311);
    tfnTraversePageToGetStylesFromHexArray(12, 29, 20, styles, 8439);
  }
END_TEST


/* Depends on getHeldGraphic(), although that is trivially simple. */
START_TEST(test_setHeldGraphic){
    setHeldGraphic(FONT_INDEX_FOR_SPACE, 1);
    ck_assert(getHeldGraphic()->lastGraphic == FONT_INDEX_FOR_SPACE);
    ck_assert(getHeldGraphic()->lastSepState == 1);

    setHeldGraphic(FONT_INDEX_FOR_SPACE + 1, 0);
    ck_assert(getHeldGraphic()->lastGraphic == FONT_INDEX_FOR_SPACE + 1);
    ck_assert(getHeldGraphic()->lastSepState == 0);
  }
END_TEST


/* A function for the extension. */
START_TEST(test_collectNumber){
    unsigned long int inputComplete;
    char typedNum;

    typedNum = '1';
    inputComplete = 0;
    ck_assert(assembleNumber(typedNum, inputComplete) == 1);
    ck_assert(assembleNumber(typedNum, inputComplete) == 11);
    ck_assert(assembleNumber(typedNum, inputComplete) == 111);
    /* Overflow test case */
    ck_assert(assembleNumber(typedNum, inputComplete) == 111);

    typedNum = '0';
    ck_assert(assembleNumber(typedNum, inputComplete) == 110);
    ck_assert(assembleNumber(typedNum, inputComplete) == 100);
    ck_assert(assembleNumber(typedNum, inputComplete) == 0);
    typedNum = '1';
    ck_assert(assembleNumber(typedNum, inputComplete) == 1);
    ck_assert(assembleNumber(typedNum, inputComplete) == 11);

    /* Append enabled only */
    inputComplete = 1;
    typedNum = '2';
    ck_assert(assembleNumber(typedNum, inputComplete) == 2);
    inputComplete = 0;
    ck_assert(assembleNumber(typedNum, inputComplete) == 22);
    typedNum = '0';
    inputComplete = 0;
    ck_assert(assembleNumber(typedNum, inputComplete) == 220);
  }
END_TEST


Suite *makeMasterSuite(void)
{
  Suite *s = suite_create ("Master");
  return s;
}


Suite *makeSuiteForLoadIn(void)
{
  Suite *s = suite_create("LoadIn");
  TCase *tc_core = tcase_create("core");
  /* TCase *tc_limits = tcase_create("limits") */

  /* Adds tests to the 'tc_core' testcase. */
  tcase_add_test(tc_core, test_validateArgCount);
  tcase_add_test(tc_core, test_validateHexFile);
  tcase_add_test(tc_core, test_readHexFileIntoString);
  tcase_add_test(tc_core, test_populateArrayWithHexString);
  tcase_add_test(tc_core, test_getCodesFromHexArray);
  tcase_add_test(tc_core, test_generateStrFromInt);
  tcase_add_test(tc_core, test_generateFilePath);
  tcase_add_test(tc_core, test_translateHexCodeToFontIndex);

  /* Adds tc_core to the testcases defined by the suite. */
  suite_add_tcase(s, tc_core);
  /* suite_add_tcase(s, limits); */

  return s;
}


void setupSDL(void)
{
  /* Actually unusable because it depends on global variables, which are forbidden by the style guide,
   * and sharing simplewin and surfaces/textures between tests. */
}


void teardownSDL(void)
{
  SDL_Quit();
}


Suite *makeSuiteThatUsesSDL(void)
{
  Suite *s = suite_create ("UsesSDL");
  TCase *tc_core = tcase_create("core");

  /* Adds tests to the 'tc_core' testcase. */
  /* Adds a 'test fixture' (a common setupSDL and clean-up routine) */
  tcase_add_unchecked_fixture(tc_core, setupSDL, teardownSDL);
  tcase_add_test(tc_core, test_SDL_InitWrapper);
  tcase_add_test(tc_core, test_loadGlyphsFromFolderIntoImageFont);
  tcase_add_test(tc_core, test_populateImageFont);
  tcase_add_test(tc_core, test_IMG_LoadToSurface);
  tcase_add_test(tc_core, test_surfaceToTexture);
  tcase_add_test(tc_core, test_freeImageFont);
  /* A function only used in the extension. */
  tcase_add_test(tc_core, test_keyup);
  /* A function mostly used in the extension. */
  tcase_add_test(tc_core, test_handleEvent);
  /* Remaining functions copyGlyphTextureToRenderer(), drawCellFG() and traverseAndDrawTeletextPage()
   * are best tested visually, as they draw to screen. */

  /* Adds tc_core to the testcases in the suite. */
  suite_add_tcase(s, tc_core);

  return s;
}


Suite *makeSuiteForStyles(void)
{
  Suite *s = suite_create ("Styles");
  TCase *tc_core = tcase_create("core");

  /* Adds tests to the 'tc_core' testcase. */
  /* Adds a 'test fixture' (a common setupSDL and clean-up routine) */
  tcase_add_test(tc_core, test_setStylesToDefault);
  tcase_add_test(tc_core, test_convertNoOfBitsToBitPattern);
  tcase_add_test(tc_core, test_getRGBBitPattern);
  tcase_add_test(tc_core, test_getControlCodeInput);
  tcase_add_test(tc_core, test_getBitsInRegion);
  tcase_add_test(tc_core, test_getRegionToShiftTo);
  tcase_add_test(tc_core, test_generateToolsForSettingStylesBit);
  /* Assumes that generateToolsForSettingStylesBit() is already working. */
  tcase_add_test(tc_core, test_setCell);
  /* Assumes that setCell() and getStyleFromHexCode() are already working. */
  tcase_add_test(tc_core, test_setRow);
  tcase_add_test(tc_core, test_setStyleBits);
  /* Decided to stop testing functions that are just switches, so no test for getActivityOfControlCode. */
  tcase_add_test(tc_core, test_getStyle);
  tcase_add_test(tc_core, test_getRGB);
  /* Deciding not to test getIncomingGfxMode() and getIncomingHeightMode(), as they are just switches. */
  tcase_add_test(tc_core, test_getIncomingMode);
  tcase_add_test(tc_core, test_getStyleFromHexCode);
  tcase_add_test(tc_core, test_traversePageToGetStylesFromHexArray);
  /* Depends on getHeldGraphic(), although this is trivially simple. */
  tcase_add_test(tc_core, test_setHeldGraphic);
  /* A function used in the extension. */
  tcase_add_test(tc_core, test_collectNumber);


  /* Adds tc_core to the testcases in the suite. */
  suite_add_tcase(s, tc_core);

  return s;
}



int main(void)
{
  int number_failed;
  SRunner *theRunner;
  FILE *silenceStdErr = fopen("testErrStream.txt", "wb");

  setErrorStream(silenceStdErr);

  /* Creates a suite runner, which is aware of all suites added into it.
   * MasterSuite is an empty suite just to allow theRunner to be allocated. */
  theRunner = srunner_create(makeMasterSuite());

  /* Adds suites into theRunner's list of suites to run. */
  srunner_add_suite (theRunner, makeSuiteForLoadIn());
  srunner_add_suite (theRunner, makeSuiteThatUsesSDL());
  srunner_add_suite (theRunner, makeSuiteForStyles());

  /* Runs all the suites in theRunner's list of suites. 'CK_VERBOSE' can be used to print any passes. */
  srunner_run_all(theRunner, CK_VERBOSE);
  number_failed = srunner_ntests_failed(theRunner);
  srunner_free(theRunner);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
