/* Decodes the magazine & page numbers of, and also demultiplexes the concurrently sent interleaved packets of Teletext
 * pages belonging to different magazines, from a real recorded broadcast:
 * In the recorded broadcast, a few packets belonging to one magazine would be sent in sequence, but then they would be
 * followed by a sequence of a few belonging to another magazine before the sequence of the initial magazine would be resumed.
 *
 * NOTE: ignores hamming-encoded protection bits, so does not correct for errors in transmission of bits. */

#include "demux.h"

/* MISALIGN_OF_BROADCAST refers to the broadcast being offset; the offset could not be dynamically extracted, as the data
 * conveying the broadcast repeat-points was annoyingly removed from the broadcast recording. Instead, it was deduced
 * through manual tinkering in a hex editor until packets lined up in a regular fashion. */
RewindNeeded getNextPacket(FILE *broadcastFilepath, unsigned char *packet)
{
  size_t bytesReceived;

  bytesReceived = fread(packet, sizeof(char), packetLength, broadcastFilepath);
  /* displayPacket(packet); Big print of all packet metadata. */
  if(bytesReceived < packetLength){
    fseek(broadcastFilepath, MISALIGN_OF_BROADCAST, SEEK_SET);

    bytesReceived = fread(packet, sizeof(char), packetLength, broadcastFilepath);
/*    displayPacket(packet); */
    if(bytesReceived < packetLength){
      fprintf(*getErrorStream(), "Broadcast file was too short to contain even a"\
        " packet. \n");
      fflush(*getErrorStream());
      return CANT_FIND_IN_BROADCAST;
    }
    /* We got a packet but had to rewind the file to get it */
    fseek(broadcastFilepath, MISALIGN_OF_BROADCAST, SEEK_SET);
/*    printf("File rewound!\n"); */
    return REWIND_NECESSITATED;
  }
  /* We got a packet without having to rewind */
  return FOUND_WITHOUT_REWINDING;
}


RewindNeeded intToEnumForRewindNeeded(int rewindNeeded)
{
  switch(rewindNeeded){
    case 0:
      return FOUND_WITHOUT_REWINDING;
    case 1:
      return REWIND_NECESSITATED;
    default:
      return CANT_FIND_IN_BROADCAST;
  }
}


/* Seeks through the broadcast stream until it finds a packet belonging to the target
 * magazine; returns whether it had to rewind once or twice (signifying that the
 * target is not available) in the process. */
RewindNeeded getNextPacketOfMagazine(FILE *broadcastFilepath, unsigned char *packet,
    int targetMagazine)
{
  int magazine, result = FOUND_WITHOUT_REWINDING;
  RewindNeeded enumResult;

  do{
    result += getNextPacket(broadcastFilepath, packet);
    if(result >= CANT_FIND_IN_BROADCAST){
      /* Broadcast too short to get a packet, or target packet wasn't found despite
       * two rewinds. */
      return CANT_FIND_IN_BROADCAST;
    }
    magazine = getMagazine(packet[BYTE4]);
  }
  while(magazine != targetMagazine);

  enumResult = intToEnumForRewindNeeded(result);

  /* If returning 1, means that a rewind occurred, so the page-assembler needs to
   *throw away what was already assembled.
   * Returns 0 if we got a packet from our target magazine without having to rewind. */
  return enumResult;
}


/* Note: the header uniquely informs of the page number, unlike the body packets,
 *which just inform of the magazine and
 * packet number. */
RewindNeeded getPageOfMagazine(FILE *broadcastFile, int userInput, unsigned char *rawPage)
{
  unsigned char packet[packetLength];
  char radix = DECIMAL;
  int i,
      packetNumber = -1,
      pageUnits,
      pageTens,
      pageNumber,
      targetPageNumber = (userInput % (radix * radix)),
      targetMagazineNumber = (userInput - targetPageNumber) / (radix * radix),
      result,
      rewindCount = 0,
      highestPacketReceived;

  /* The range of viewable Teletext pages is magazines 1-8. Ref: Section 2.1.3 of
   * 1974 Teletext spec. */
  if(targetMagazineNumber == 0 || targetMagazineNumber == 9){
    memset(rawPage, UNREADABLE_CHAR, ROWS * COLUMNS);
    return FOUND_WITHOUT_REWINDING;
  }

  while(rewindCount < CANT_FIND_IN_BROADCAST && packetNumber != 0){
    pageNumber = -1;

    /* Find the header packet for the page (and magazine) we're interested in and
     * extract the page number from it. */
    while(targetPageNumber != pageNumber && rewindCount < CANT_FIND_IN_BROADCAST){

      rewindCount += result = getNextPacketOfMagazine(broadcastFile, packet,
          targetMagazineNumber);
/*      displayPacket(packet); */
      if(result == 0){
        packetNumber = getPacketNumber(packet[BYTE4], packet[BYTE5]);

        if(packetNumber == 0) {
          pageUnits = getPageUnits(packet[BYTE6]);
          pageTens = getPageTens(packet[BYTE7]);
          pageNumber = (pageTens * radix) + pageUnits;
/*          printf("Start of page %d found \n", pageNumber); */
        }
      }
    }

    if(rewindCount >= CANT_FIND_IN_BROADCAST){
      fprintf(*getErrorStream(), "Target header packet for entry %d was not found"\
      " within broadcast. \n", userInput);
      fflush(*getErrorStream());
      memset(rawPage, UNREADABLE_CHAR, ROWS * COLUMNS);
      return CANT_FIND_IN_BROADCAST;
    }

    /* From this stage, we've found the header packet corresponding to the page
     * and magazine of interest: */

    for(i = BYTE6; i < (BYTE6 + NON_CODING_CHARS_IN_HEADER); i++){
      packet[i] = (char)HEXCODE_FOR_SPACE;
    }
    memcpy(rawPage, &packet[BYTE6], COLUMNS);

    packetNumber = -1;
    result = 0;
    highestPacketReceived = -1;

    while(packetNumber != 0 && rewindCount < CANT_FIND_IN_BROADCAST && result == 0){
      rewindCount += result = getNextPacketOfMagazine(broadcastFile, packet,
          targetMagazineNumber);

      if(result == 0){
        packetNumber = getPacketNumber(packet[BYTE4], packet[BYTE5]);

        if(packetNumber <= highestPacketReceived){
          /* If the broadcast stops giving a higher packet number, fall through
           * the following conditions.
           * This is crucial because the broadcast sometimes starts sending a new
           * page on the same magazine without finishing the initial one. */
          packetNumber = 0;
        }

        if(packetNumber > 0 && packetNumber < ROWS){
/*          displayPacket(packet); */
          memcpy(rawPage + packetNumber * COLUMNS, &packet[BYTE6], COLUMNS);
          highestPacketReceived = packetNumber;
        }
      }

      /* If rewind interrupted the page assembly, throw the page away and start
      again from the header acquisition. */
      if (result == REWIND_NECESSITATED) {
        memset(rawPage, UNREADABLE_CHAR, ROWS * COLUMNS);
      }

      /* If we've been through the file twice without finding the body packet: */
      if(rewindCount >= CANT_FIND_IN_BROADCAST){
        fprintf(*getErrorStream(), "No body packets for entry %d were not found"\
        " within broadcast. \n", userInput);
        fflush(*getErrorStream());
        memset(rawPage, UNREADABLE_CHAR, ROWS * COLUMNS);
        return CANT_FIND_IN_BROADCAST;
      }
    }


  }

  return FOUND_WITHOUT_REWINDING;
}

/* Just a debugging function; no longer need to print out metadata of packets.
void displayPacket(unsigned char *packet)
{
  char radix = 10;
  int i,
      packetNumber,
      pageUnits,
      pageTens,
      magazine,
      pageNumber;

  magazine = getMagazine(packet[BYTE4]);
  packetNumber = getPacketNumber(packet[BYTE4], packet[BYTE5]);
  pageUnits = getPageUnits(packet[BYTE6]);
  pageTens = getPageTens(packet[BYTE7]);
  pageNumber = (pageTens * radix) + pageUnits;

  if(packetNumber == 0){
    printf("H ");
    for (i = 0; i < 20; i++) {
      printf("%02x", packet[i]);
    }
    printf("%2d %2d %3d ", magazine, packetNumber, pageNumber);
    for (i = 34; i < 44; i++) {
      printf("%c", packet[i] & 0x7f);
    }
    printf("\n");
  }
  else if(packetNumber > 0 && packetNumber < 25){
    printf("P ");

    for (i = 0; i < 20; i++) {
      printf("%02x", packet[i]);
    }
    printf("%2d %2d   ", magazine, packetNumber );

    for (i = BYTE6; i < 44; i++) {
      int ch = packet[i] & 0x7f;

      if (isprint(ch)) {
        printf("%c", ch);
      }
      else {
        printf(" ");
      }
    }
    printf("\n");
  }
}
*/

/* No longer using this functionality.
long int getFileLen(FILE* file)
{
  long int fileLen;

  fseek(file, 0L, SEEK_END);
  fileLen = ftell(file);
  fseek(file, 0L, SEEK_SET);

  return fileLen;
}
*/

int getMagazine(char byte4OfPacket)
{
  int magazine = 0;

  magazine = ((byte4OfPacket & BIT_1) >> SHIFT_TO_BIT_1_OF_MAGAZINE)
             + ((byte4OfPacket & BIT_3) >> SHIFT_TO_BIT_2_OF_MAGAZINE)
             + ((byte4OfPacket & BIT_5) >> SHIFT_TO_BIT_3_OF_MAGAZINE);

  return magazine;
}


int getPacketNumber(char byte4OfPacket, char byte5OfPacket)
{
  int packetNumber = 0;

  packetNumber = ((byte4OfPacket & BIT_7) >> SHIFT_TO_BIT_1_OF_PACKETNO)
                 + ((byte5OfPacket & BIT_1) >> SHIFT_TO_BIT_2_OF_PACKETNO)
                 + ((byte5OfPacket & BIT_3) >> SHIFT_TO_BIT_3_OF_PACKETNO)
                 + ((byte5OfPacket & BIT_5) >> SHIFT_TO_BIT_4_OF_PACKETNO)
                 + ((byte5OfPacket & BIT_7) >> SHIFT_TO_BIT_5_OF_PACKETNO);

  return packetNumber;
}


int getPageUnits(char byte6OfPacket)
{
  int pageUnits = 0;

  pageUnits = ((byte6OfPacket & BIT_1) >> SHIFT_TO_BIT_1_OF_PAGEUNITS)
              + ((byte6OfPacket & BIT_3) >> SHIFT_TO_BIT_2_OF_PAGEUNITS)
              + ((byte6OfPacket & BIT_5) >> SHIFT_TO_BIT_3_OF_PAGEUNITS)
              + ((byte6OfPacket & BIT_7) >> SHIFT_TO_BIT_4_OF_PAGEUNITS);

  return pageUnits;
}


int getPageTens(char byte7OfPacket)
{
  int pageTens = 0;

  pageTens = ((byte7OfPacket & BIT_1) >> SHIFT_TO_BIT_1_OF_PAGETENS)
             + ((byte7OfPacket & BIT_3) >> SHIFT_TO_BIT_2_OF_PAGETENS)
             + ((byte7OfPacket & BIT_5) >> SHIFT_TO_BIT_3_OF_PAGETENS)
             + ((byte7OfPacket & BIT_7) >> SHIFT_TO_BIT_4_OF_PAGETENS);

  return pageTens;
}


/* No longer used, due to single-thread limitations */
char getWhetherInputComplete(unsigned long int ticksAtCurrentCall)
{
  char inputComplete = 1;
  static unsigned long int ticksAtLastCall = 0;

  if(ticksAtCurrentCall - ticksAtLastCall < ONE_SECOND) {
    inputComplete = 0;
  }

  ticksAtLastCall = ticksAtCurrentCall;

  return inputComplete;
}


/* Only appends to existing number if typed within one second. */
/* No longer used, due to single-thread limitations; see instead simpleAssembleNumber(). */
int assembleNumber(char typedNum, char inputComplete)
{
  static int currentlyAssembledNo = 0;
  int radix = DECIMAL, typedNumAsInt = typedNum - '0';

  if(inputComplete){
    currentlyAssembledNo = typedNumAsInt;
  }
  else{
    currentlyAssembledNo = (currentlyAssembledNo * radix) % MAX_TELETEXT_PAGES;
    currentlyAssembledNo += typedNumAsInt;
  }

  return currentlyAssembledNo;
}


int simpleAssembleNumber(char typedNum)
{
  static int currentlyAssembledNo = 0;
  int radix = DECIMAL, typedNumAsInt = typedNum - '0';

  /* Only append to existing number if typed within one second. */
  currentlyAssembledNo = (currentlyAssembledNo * radix) % MAX_TELETEXT_PAGES;
  currentlyAssembledNo += typedNumAsInt;

  return currentlyAssembledNo;
}
