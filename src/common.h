#ifndef MYTEST_COMMON_H
#define MYTEST_COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL_image.h>

/* Read-in stuff */
#define ROWS 25
#define COLUMNS 40
#define DENOMINATOR_FOR_HALF 2
#define BINARY 2
#define DECIMAL 10
#define NULLCHAR '\0'
#define ENTER '\n'
#define NEXT_PAGE '+'
#define PREV_PAGE '-'
#define LENGTH_OF_NULLCHAR 1
#define PARITY_BIT (1 << 7)
#define CHARACTERS_IN_FILE_EXTENSION 3
#define POS_DECIDING_NO_OF_DIGITS_TO_PAD_BY 2
#define THREE_DIGIT_ID 3
#define CHARS_IN_IMG_FILE_EXTENSION 4
#define CHARS_IN_HEXFILE_EXTENSION 3
#define BASEPATH_TO_GFX "assets/imgfnt/gfx/"
#define BASEPATH_TO_ALPHA "assets/imgfnt/alpha/"
#define BASEPATH_TO_ASSETS_FOLDER "assets/"
#define BASEPATH_TO_OTHER_HEX_FILES "assets/"

/* Font stuff */
#define FIRST_PRINTABLE_GLYPH 32
#define FIRST_SHARED_GLYPH_FILENUMBER 64
#define TOTAL_GLYPHS_IN_MODE 96
#define RESUMED_GFX_GLYPH_FILENUMBER 96
#define DEFAULT_CODE 119
#define HIGHEST_GLYPH_CODE 127
#define BACKGROUND_GLYPH_INDEX 159
#define GLYPHS_NEEDED (32 * 5)
#define START_PRINTABLE_HEXCODES 0xa0
#define FONT_INDEX_FOR_SPACE 0
#define SEPARATING_GRID_INDEX 96
#define HEXCODE_FOR_SPACE 0xa0
#define FIRST_SHARED_GLYPH_HEXCODE 0xc0
#define RESUMED_GFX_GLYPH_HEXCODE 0xe0

/* Rendering stuff */
#define GLYPH_WIDTH 12
#define GLYPH_HEIGHT 20
#define BODY_WIDTH (GLYPH_WIDTH * COLUMNS)
#define BODY_HEIGHT (GLYPH_HEIGHT * ROWS)
#define FULL_RGB 255
#define HEXCODE_OF_FIRST_PRINTABLE_GLYPH_WITH_PARITY (FIRST_PRINTABLE_GLYPH | PARITY_BIT)

/* Header re-writing stuff */
#define START_OF_SDL_NUMBER_SCANCODES 30
#define HEXCODE_FOR_ZERO 0x30
#define HEXCODE_FOR_P 0x50
#define HEXCODE_FOR_DASH 0x60
#define COLUMN_OF_P 2
#define COLUMN_OF_MAGAZINE 3
#define MAX_TELETEXT_PAGES 1000
#define THREE_DIGIT_NO 3
#define ASSEMBLED_NO_TO_MAGAZINE 100
#define LANDING_PAGE 100


enum WhichAssetFolderToLoadFrom {
  LOAD_FROM_ALPHA_FOLDER,
  LOAD_FROM_GFX_FOLDER
};
typedef enum WhichAssetFolderToLoadFrom WhichAssetFolderToLoadFrom;

enum ExtensionOrBasic {
  BASIC,
  EXTENSION
};
typedef enum ExtensionOrBasic ExtensionOrBasic;

enum SetWhiteToBeAlphaChannel {
  DONT_SET_WHITE_AS_ALPHA,
  DO_SET_WHITE_AS_ALPHA
};
typedef enum SetWhiteToBeAlphaChannel SetWhiteToBeAlphaChannel;

enum ControlCodesIncParityBit {
  A_RED = 0x81,
  A_GREEN,
  A_YELLOW,
  A_BLUE,
  A_MAGENTA,
  A_CYAN,
  A_WHITE,
  SINGLE_HEIGHT = 0x8C,
  DOUBLE_HEIGHT,
  GFX_RED = 0x91,
  GFX_GREEN,
  GFX_YELLOW,
  GFX_BLUE,
  GFX_MAGENTA,
  GFX_CYAN,
  GFX_WHITE,
  CONTIG_GFX = 0x99,
  SEP_GFX,
  BG_BLACK = 0x9C,
  BG_NEW,
  HOLD_GFX,
  RELEASE_GFX
};

/* Binary extensions turn out to be a GCC extension, so here are the original binary values:
  BLACK = 0b000,
  BLUE = 0b001,
  GREEN = 0b010,
  CYAN = 0b011,
  RED = 0b100,
  MAGENTA = 0b101,
  YELLOW = 0b110,
  WHITE = 0b111,
  TOGGLE_OFF = 0b0,
  TOGGLE_ON = 0b1,
  NULL_INPUT = 0b1111,
  NEW_BG_REQUESTED = 0b1000
*/
enum ControlCodeInputs {
  BLACK,
  BLUE,
  GREEN,
  CYAN,
  RED,
  MAGENTA,
  YELLOW,
  WHITE,
  NEW_BG_REQUESTED,
  TOGGLE_OFF = 0,
  TOGGLE_ON,
  NULL_INPUT = 15
};
typedef enum ControlCodeInputs ControlCodeInputs;

enum BitsInRegion {
  NULL_REGION = 0,
  SINGLE_BIT_REGION,
  RGB_REGION = 3
};
typedef enum BitsInRegion BitsInRegion;

enum Mode {
  NO_CHANGE_IN_MODE = -1,
  MODE_DISABLED,
  MODE_ENABLED
};
typedef enum Mode Mode;

enum CCodeActivity {
  IS_NOT_ACTIVE_CONTROL_CODE,
  IS_ACTIVE_CONTROL_CODE
};
typedef enum CCodeActivity CCodeActivity;

enum ModePassedByCurrentHexCode {
  HEIGHT_MODE,
  ALPHA_OR_GFX_MODE
};
typedef enum ModePassedByCurrentHexCode ModePassedByCurrentHexCode;

enum ShiftToRegion {
  SHIFT_TO_A_RGB_REGION,
  SHIFT_TO_D_HEIGHT_REGION = 3,
  SHIFT_TO_GFX_RGB_REGION,
  SHIFT_TO_SEP_GFX_REGION = 7,
  SHIFT_TO_BG_RGB_REGION,
  SHIFT_TO_HOLD_GFX_REGION = 11,
  SHIFT_TO_CLEAR_HELD_GFX_REGION,
  SHIFT_TO_GFX_MODE_REGION,
  SHIFT_TO_NULL_REGION
};
typedef enum ShiftToRegion ShiftToRegion;

enum ImgOrHexfile {
  IMG,
  HEXFILE
};
typedef enum ImgOrHexfile ImgOrHexfile;


struct SDL_Simplewin {
/*  SDL_bool finished; */
  SDL_Window *win;
  SDL_Renderer *renderer;
};
typedef struct SDL_Simplewin SDL_Simplewin;

struct RGBCode {
  Uint8 r;
  Uint8 g;
  Uint8 b;
};
typedef struct RGBCode RGBCode;

struct HeldGraphicAndStyle {
  unsigned char lastGraphic;
  unsigned char lastSepState;
};
typedef struct HeldGraphicAndStyle HeldGraphicAndStyle;

struct StyleFromHexCode {
  unsigned short input;
  BitsInRegion bitsInRegion;
  ShiftToRegion bitShift;
  unsigned char alphaOrGfxMode;
  unsigned char clearHeldGfx;
};
typedef struct StyleFromHexCode StyleFromHexCode;

struct SetBitTools {
  unsigned short inputBitMask;
  unsigned short inputShifted;
};
typedef struct SetBitTools SetBitTools;

struct AssembledNoMeta {
  int currentlyAssembledNo;
  char inputComplete;
};
typedef struct AssembledNoMeta AssembledNoMeta;


FILE** getErrorStream();
void setErrorStream(FILE* fpp);
int validateArgCount(int argc, int ARGS_NEEDED);
void exitIfIntTestAboveZero(int result);
void exitIfPointerTestFailed(void *result);
FILE *validateHexFile(char *filePath);
unsigned char *readHexFileIntoString(unsigned char rawPage[ROWS * COLUMNS],
    FILE *pHexFileFilepath);
void populateArrayWithHexString(unsigned char *hexFileAsString,
    unsigned char array[ROWS][COLUMNS]);
int SDL_InitWrapper(SDL_Simplewin* sw);
char* generateStrFromInt(int i, int digitsOfID, ImgOrHexfile imgOrHexfile);
char* generateFilePath(unsigned int i, char *basePath, ImgOrHexfile imgOrHexfile);
unsigned int loadGlyphsFromFolderIntoImageFont(SDL_Texture* imageFont[GLYPHS_NEEDED],
    SDL_Simplewin* sw, unsigned int arrayPosToLoadInto,
    WhichAssetFolderToLoadFrom folderToLoadFrom);
int populateImageFont(SDL_Texture **imageFont, SDL_Simplewin *sw);
SDL_Surface* IMG_LoadToSurface(char *filepath, SetWhiteToBeAlphaChannel setWhiteAsAlpha);
SDL_Texture* surfaceToTexture(SDL_Simplewin* sw, SDL_Surface* surface);
void setStylesToDefault(unsigned short styles[ROWS][COLUMNS]);
unsigned short convertNoOfBitsToBitPattern(BitsInRegion bitsInRegion,
    unsigned char radix);
unsigned short getRGBBitPattern(unsigned short currentStyle,
    ShiftToRegion shiftToWhichRegion);
ControlCodeInputs getControlCodeInput(unsigned char controlCode);
Mode getIncomingGfxMode(unsigned char currentHexCode);
Mode getIncomingHeightMode(unsigned char currentHexCode);
char getIncomingMode(ModePassedByCurrentHexCode modePassedByCurrentHexCode,
    unsigned char currentHexCode, unsigned char currentMode);
SetBitTools generateToolsForSettingStylesBit(unsigned short input,
    BitsInRegion bitsInRegion, ShiftToRegion bitShift);
void setRow(unsigned short styles[ROWS][COLUMNS], int currentRow, int currentColumn,
    StyleFromHexCode styleFromHexCode);
unsigned short setStyleBits(unsigned short styles[ROWS][COLUMNS], int currentRow,
    int currentColumn, unsigned short inputBitMask, unsigned short input);
unsigned short setCell(unsigned short styles[ROWS][COLUMNS], int currentRow,
    int currentColumn, SetBitTools toolsForSettingBitsOfInput,
    SetBitTools toolsForSettingBitsOfGfxMode);
BitsInRegion getBitsInRegion(unsigned char controlCode);
ShiftToRegion getRegionToShiftTo(unsigned char controlCode);
unsigned char translateHexCodeToFontIndex(unsigned char hexCode,
    unsigned char graphicsModeOn);
CCodeActivity getActivityOfControlCode(unsigned char currentHexCode);
void traversePageToGetStylesFromHexArray(unsigned char frameBuffer[ROWS][COLUMNS],
    unsigned short styles[ROWS][COLUMNS]);
StyleFromHexCode getStyleFromHexCode(unsigned char currentHexCode,
    unsigned short currentStyle);
RGBCode getRGB(unsigned short currentStyle, unsigned char shiftToWhichRGBRegion);
unsigned char getStyle(unsigned char shiftToWhichStyleRegion, unsigned short currentStyle);
HeldGraphicAndStyle *getHeldGraphic();
void setHeldGraphic(unsigned char currentGraphic, unsigned char sepGfxToggle);
void copyGlyphTextureToRenderer(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char glyphToBeDrawn, unsigned short currentStyle,
    unsigned char shiftToWhichRGBRegion, SDL_Rect srcRect, SDL_Rect rectToDrawTo);
void drawCellFG(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char currentHexCode, unsigned short currentStyle,
    unsigned short aboveStyle, SDL_Rect srcRect, SDL_Rect rectToDrawTo);
int traverseAndDrawTeletextPage(SDL_Simplewin *sw, SDL_Texture *imageFont[GLYPHS_NEEDED],
    unsigned char hexStringAsArray[ROWS][COLUMNS], unsigned short styles[ROWS][COLUMNS]);
int freeImageFont(SDL_Texture* imageFont[GLYPHS_NEEDED]);
char keyup(SDL_Event *event);
int handleEvent(SDL_Event *event, char *typedNum, ExtensionOrBasic extensionOrBasic);
void renderPageToScreen(unsigned char rawPage[ROWS * COLUMNS],
    unsigned char frameBuffer[ROWS][COLUMNS], int assembledNo,
    unsigned short styles[ROWS][COLUMNS], SDL_Simplewin* sw, SDL_Texture *imageFont[GLYPHS_NEEDED]);
void overwriteHeader(unsigned char frameBuffer[ROWS][COLUMNS], int assembledNo);

#endif /*MYTEST_COMMON_H*/
