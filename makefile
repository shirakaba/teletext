# Please run the clean command detailed at the bottom before running: make -f makefile

LFLAGS = -g $(LOCAL_LFLAGS) -lSDL2 -lSDL_image -lm
COMMON_FLAGS = -pedantic -Wall -Wextra -Wfloat-equal -O2 -lm
#C99 needed, just in test suite, for anonymous variadic macros.
TEST_FLAGS = $(COMMON_FLAGS) -std=c99 -lcheck $(LFLAGS)
SOURCE_OR_EXTNSN_FLAGS = $(COMMON_FLAGS) -ansi $(LFLAGS)
LOCAL_LFLAGS = -Iinclude -Llib -Wl,-rpath,lib
DELETE = rm
DELETE_RECURSIVE = rm -r

# List all the files here
ASSETS_CONTENTS = assets/
COMMON_FILES = common.o
SOURCE_FILES = teletext.o $(COMMON_FILES)
EXTENSION_FILES = extension.o demux.o $(COMMON_FILES)
TEST_FILES = test.o demux.o $(COMMON_FILES)
BIN = bin
PROG = $(BIN)/Teletext
EXTENSION = $(BIN)/Extension
TEST = $(BIN)/Test
CXX = gcc

all: $(PROG) $(EXTENSION) $(TEST)

# compiling source files.
%.o: src/%.c src/%.h
	$(CXX) $(SOURCE_OR_EXTNSN_FLAGS) -c -s $<
test.o: src/test.c src/test.h
	$(CXX) $(TEST_FLAGS) -c -s $<

# linking the files together into their respective programs. | $(BIN) makes the bin/ directory if it does not yet exist.
$(PROG): $(SOURCE_FILES) | $(BIN)
	$(CXX) $(SOURCE_FILES) -o $(PROG) $(SOURCE_OR_EXTNSN_FLAGS)
$(EXTENSION): $(EXTENSION_FILES) | $(BIN)
	$(CXX) $(EXTENSION_FILES) -o $(EXTENSION) $(SOURCE_OR_EXTNSN_FLAGS)
$(TEST): $(TEST_FILES) | $(BIN)
	$(CXX) $(TEST_FILES) -o $(TEST) $(TEST_FLAGS)
# $(TEST_FLAGS) used to give the directory by which to include Check, but this conflicted with linking libGL.
# Now libGL, SDL_image, and (if necessary later, SDL_ttf) are safely copied into the local lib folder.

# makes the bin/ directory and copies assets to bin/ so we don't have to drag them in manually.
# $@ is merely a variable provided by makefile which effectively substitutes in the rule "$(BIN)".
$(BIN):
	mkdir $@
	cp -R $(ASSETS_CONTENTS) $@

# cleans everything that can be automatically recreated with "make".
# To do this, run the following command in verdant-octo-spork/Prototype: make -f makefile clean
clean:
	$(DELETE) *.o
	$(DELETE_RECURSIVE) $(BIN)
